$(document).ready(function () {
    $('.filter-show').click(function () {
        $(this).closest('th').find('.saw-filters').toggleClass('hidden');
    });

    $('.select-search').change(function () {
        $('.search-go-btn').click();
    });

    $(".date-picker").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect : function(selected_date) {
            $('.search-go-btn').click();
        }
    });

    if ($(window).width() < 991) {
        $('.filter-reset').click(function () {
            $(this).closest('li').find('.select-search, .date-picker').val('');
            $('.search-go-btn').click();
        });
    } else {
        $('.filter-reset').click(function () {
            $(this).closest('th.text-center').find('.select-search, .date-picker').val('');
            $('.search-go-btn').click();
        });
    }
});
