$('.navbar-nav li a').on('click touchend', function(e) {
    var el = $(this);
    var link = el.attr('href');
    window.location = link;
});

$('.navbar-toggle').on('click',function () {
    if($('.navbar-toggle').hasClass('open')) {
        $('.fix-left').animate({left: '-300'});
        $(this).removeClass('open');
    } else {
        $('.fix-left').animate({left: '0'});
        $(this).addClass('open');
    }
});

if ($(window).width() < 991) {
    $('.custom-table').find('select, input').attr('disabled', 'disabled');
}
if ($(window).width() > 991) {
    $('.responsive-filters').find('select, input').attr('disabled', 'disabled');
}

$(".responsive-filters .view-btn").on('click', function () {
   $(".responsive-filters .filter-list").toggle(400);
});