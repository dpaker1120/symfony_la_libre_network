/**
 * jQuery Form Validator Module: Security
 * ------------------------------------------
 *
 * This form validation module adds validators typically used on swedish
 * websites. This module adds the following validators:
 *  - validate_betva
 *
 */

(function ($, window) {

  $.formUtils.registerLoadedModule('betva');

  /*
   * Validate phone number, at least 7 digits only one hyphen and plus allowed
   */
  $.formUtils.addValidator({
    name: 'betva',
    validatorFunction: function (number) {

      if (number.indexOf('BE') !== 0) {
        return false;
      }

      number = number.replace(/([-|\+])/g, '');
      return number.length > 9 && number.match(/[^0-9]/g) === null;
    },
    errorMessage: '',
    errorMessageKey: 'badBEVatAnswer'
  });


})(jQuery, window);
