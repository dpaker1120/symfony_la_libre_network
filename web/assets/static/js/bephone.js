/**
 * jQuery Form Validator Module: Security
 * ------------------------------------------
 *
 * This form validation module adds validators typically used on swedish
 * websites. This module adds the following validators:
 *  - validate_bephone
 *
 */

(function ($, window) {

  $.formUtils.registerLoadedModule('bephone');

  /*
   * Validate phone number, at least 7 digits only one hyphen and plus allowed
   */
  $.formUtils.addValidator({
    name: 'bephone',
    validatorFunction: function (tele) {
      var numPlus = tele.match(/\+/g);
      var numMoins = tele.match(/-/g);

      if ((numPlus !== null && numPlus.length > 1) || (numMoins !== null && numMoins.length > 0) || tele.indexOf('+32') !== 0) {
        return false;
      }
    //   if (numPlus !== null && tele.indexOf('+32') !== 0) {
    //     return false;
    //   }

      tele = tele.replace(/([-|\+])/g, '');
      return tele.length > 8 && tele.match(/[^0-9]/g) === null;
    },
    errorMessage: '',
    errorMessageKey: 'badTelephone'
  });


})(jQuery, window);
