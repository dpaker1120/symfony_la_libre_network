document.documentElement.addEventListener('touchstart', function(event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, false);

jQuery(document).ready(function ($) {
    setInterval(function(){
        $('.a_btm img').toggleClass('wiggle');
        $('.a_msg img').toggleClass('wiggle');
        setTimeout(function(){
            $('.a_btm img').toggleClass('wiggle');
            $('.a_msg img').toggleClass('wiggle');
        },500);

    },1000);

    $(".a_btm").click(function(event){
        event.preventDefault();
        var offset = $($(this).attr('href')).offset().top - 95;
        $('html, body').animate({scrollTop:offset}, 500);
    });

    $(".mob_a_list a").click(function(){
        if($(this).text() === 'Voir moins') {
            $(this).text('Voir plus');
            $(".js-toggle").toggle();
            $('html, body').animate({
                scrollTop: $(".blk_2").offset().top - 400
            }, 300);
        }
        else {
            $(this).text('Voir moins');
            $(".js-toggle").toggle();
            $('html, body').animate({
                scrollTop: $(".js-toggle").offset().top - 400
            }, 300);
        }
    });


    var width = $(window).width();
    if (width > 767) {
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 200) {
                $('.header').addClass('fixed');
            } else {
                $('.header').removeClass('fixed');
            }
        });
    }

    $('.fleft').viewportChecker({
        classToAdd: 'visible animated fadeInLeft',
        offset: 100
    });

    $('.fright').viewportChecker({
        classToAdd: 'visible animated fadeInRight',
        offset: 100
    });

    $('.fup').viewportChecker({
        classToAdd: 'visible animated fadeInUp',
        offset: 100
    });


    $("#owl-demo").owlCarousel({
        navigation : false,
        slideSpeed : 300,
        paginationSpeed : 400,
        loop:true,
        center:false,
        margin:10,
        items:1.2
    });

});

$(window).load(function(){

    $.fn.extend({
        equalHeights: function(){
            var top=0;
            var row=[];
            var classname=('equalHeights'+Math.random()).replace('.','');
            $(this).each(function(){
                var thistop=$(this).offset().top;
                if (thistop>top) {
                    $('.'+classname).removeClass(classname);
                    top=thistop;
                }
                $(this).addClass(classname);
                $(this).height('auto');
                var h=(Math.max.apply(null, $('.'+classname).map(function(){ return $(this).outerHeight(); }).get()));
                $('.'+classname).height(h);
            }).removeClass(classname);
        }
    });

    $(function(){
        $(window).resize(function(){

            $('.blk_1 .one_inner .nos_blk .nos_inner').equalHeights();
            $('.equal').equalHeights();
        }).trigger('resize');
    });

});
