INSERT INTO `fos_user` (`id`, `company_id`, `customer_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `job_title`, `name`) VALUES
(1,	NULL,	NULL,	'admin',	'admin',	'admin@lalibre.com',	'admin@lalibre.com',	1,	NULL,	'$2y$13$PiyygzPj9czTKd4AhU/XC.X.cwTY19eEU1lYhl.qvH66tDpXJM6L6',	NULL,	NULL,	NULL,	'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',	'Company Job Title',	'Administrator');

INSERT INTO `subscription` (`id`, `title`, `total_articles`, `total_boosts`, `total_prints`, `total_conferences`) VALUES
(1,	'Silver',	1,	0,	0,	5),
(2,	'Gold',	1,	1,	1,	10),
(3,	'Platinium',	2,	2,	2,	20);
