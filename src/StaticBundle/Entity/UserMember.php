<?php

namespace StaticBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Subscription;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="StaticBundle\Repository\UserMemberRepository")
 */
class UserMember
{
    const STATUS_PROCESSED     = 'processed';
    const STATUS_NOT_PROCESSED = 'not_processed';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $societyName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $societyTva;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $societyLegalForm;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressPostbox;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $addressZipCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $addressCity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $nomContact;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $prenomContact;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $emailContact;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $phoneContact;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Subscription")
     */
    private $pack;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=13)
     */
    private $status = self::STATUS_NOT_PROCESSED;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSocietyName()
    {
        return $this->societyName;
    }

    /**
     * @param string $societyName
     *
     * @return UserMember
     */
    public function setSocietyName($societyName)
    {
        $this->societyName = $societyName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocietyTva()
    {
        return $this->societyTva;
    }

    /**
     * @param string $societyTva
     *
     * @return UserMember
     */
    public function setSocietyTva($societyTva)
    {
        $this->societyTva = $societyTva;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocietyLegalForm()
    {
        return $this->societyLegalForm;
    }

    /**
     * @param string $societyLegalForm
     *
     * @return UserMember
     */
    public function setSocietyLegalForm($societyLegalForm)
    {
        $this->societyLegalForm = $societyLegalForm;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     *
     * @return UserMember
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressNumber()
    {
        return $this->addressNumber;
    }

    /**
     * @param string $addressNumber
     *
     * @return UserMember
     */
    public function setAddressNumber($addressNumber)
    {
        $this->addressNumber = $addressNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressPostbox()
    {
        return $this->addressPostbox;
    }

    /**
     * @param string $addressPostbox
     *
     * @return UserMember
     */
    public function setAddressPostbox($addressPostbox)
    {
        $this->addressPostbox = $addressPostbox;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressZipCode()
    {
        return $this->addressZipCode;
    }

    /**
     * @param string $addressZipCode
     *
     * @return UserMember
     */
    public function setAddressZipCode($addressZipCode)
    {
        $this->addressZipCode = $addressZipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     *
     * @return UserMember
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomContact()
    {
        return $this->nomContact;
    }

    /**
     * @param string $nomContact
     *
     * @return UserMember
     */
    public function setNomContact($nomContact)
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenomContact()
    {
        return $this->prenomContact;
    }

    /**
     * @param string $prenomContact
     *
     * @return UserMember
     */
    public function setPrenomContact($prenomContact)
    {
        $this->prenomContact = $prenomContact;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmailContact()
    {
        return $this->emailContact;
    }

    /**
     * @param string $emailContact
     *
     * @return UserMember
     */
    public function setEmailContact($emailContact)
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneContact()
    {
        return $this->phoneContact;
    }

    /**
     * @param string $phoneContact
     *
     * @return UserMember
     */
    public function setPhoneContact($phoneContact)
    {
        $this->phoneContact = $phoneContact;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * @param Subscription $pack
     *
     * @return UserMember
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return UserMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return UserMember
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
