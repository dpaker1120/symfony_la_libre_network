<?php

namespace StaticBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserMemberRepository extends EntityRepository
{
    /**
     * @param array $searchParams
     *
     * @return \Doctrine\ORM\Query
     */
    public function getAllUserMembers(array $searchParams = [])
    {
        $qb = $this->createQueryBuilder('userMember');

        if (isset($searchParams['searchTerm']) && $searchParams['searchTerm'] != '') {
            $searchTerm = $searchParams['searchTerm'];

            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('userMember.societyName', ':enterprise'),
                    $qb->expr()->like('userMember.societyTva', ':vat'),
                    $qb->expr()->like('userMember.societyLegalForm', ':legalForm'),
                    $qb->expr()->like('userMember.nomContact', ':name'),
                    $qb->expr()->like('userMember.prenomContact', ':firstName'),
                    $qb->expr()->like('userMember.emailContact', ':email'),
                    $qb->expr()->like('userMember.phoneContact', ':phone')
                ))
                ->setParameter('enterprise', '%'.$searchTerm.'%')
                ->setParameter('vat', '%'.$searchTerm.'%')
                ->setParameter('legalForm', '%'.$searchTerm.'%')
                ->setParameter('name', '%'.$searchTerm.'%')
                ->setParameter('firstName', $searchTerm)
                ->setParameter('email', $searchTerm)
                ->setParameter('phone', $searchTerm);
        }

        $qb->orderBy('userMember.status', 'ASC');

        return $qb->getQuery();
    }
}
