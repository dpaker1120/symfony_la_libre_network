<?php

namespace StaticBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserQuestionRepository extends EntityRepository
{
    /**
     * @param array $searchParams
     *
     * @return \Doctrine\ORM\Query
     */
    public function getAllUserQuestions(array $searchParams = [])
    {
        $qb = $this->createQueryBuilder('userQuestion');

        if (isset($searchParams['searchTerm']) && $searchParams['searchTerm'] != '') {
            $searchTerm = $searchParams['searchTerm'];

            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('userQuestion.entreprise', ':enterprise'),
                    $qb->expr()->like('userQuestion.nom', ':name'),
                    $qb->expr()->like('userQuestion.prenom', ':firstName'),
                    $qb->expr()->like('userQuestion.email', ':email'),
                    $qb->expr()->like('userQuestion.phone', ':phone')
                ))
                ->setParameter('enterprise', '%'.$searchTerm.'%')
                ->setParameter('name', '%'.$searchTerm.'%')
                ->setParameter('firstName', '%'.$searchTerm.'%')
                ->setParameter('email', '%'.$searchTerm.'%')
                ->setParameter('phone', $searchTerm);
        }

        $qb->orderBy('userQuestion.status', 'ASC');

        return $qb->getQuery();
    }
}
