<?php

namespace StaticBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserCompanyRepository extends EntityRepository
{
    /**
     * @param array $searchParams
     *
     * @return \Doctrine\ORM\Query
     */
    public function getAllUserCompanies(array $searchParams = [])
    {
        $qb = $this->createQueryBuilder('userCompany');

        if (isset($searchParams['searchTerm']) && $searchParams['searchTerm'] != '') {
            $searchTerm = $searchParams['searchTerm'];

            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('userCompany.entreprise', ':enterprise'),
                    $qb->expr()->like('userCompany.nom', ':name'),
                    $qb->expr()->like('userCompany.prenom', ':firstName'),
                    $qb->expr()->like('userCompany.email', ':email'),
                    $qb->expr()->eq('userCompany.phone', ':phone')
                ))
                ->setParameter('enterprise', '%'.$searchTerm.'%')
                ->setParameter('name', '%'.$searchTerm.'%')
                ->setParameter('firstName', '%'.$searchTerm.'%')
                ->setParameter('email', '%'.$searchTerm.'%')
                ->setParameter('phone', $searchTerm);
        }

        $qb->orderBy('userCompany.status', 'ASC');

        return $qb->getQuery();
    }
}
