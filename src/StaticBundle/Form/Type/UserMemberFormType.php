<?php

namespace StaticBundle\Form\Type;

use EntityBundle\Entity\Subscription;
use FrontBundle\Services\RequestAwarePriceFormatter;
use Nietonfir\Google\ReCaptchaBundle\Form\Type\ReCaptchaType;
use StaticBundle\Entity\UserMember;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserMemberFormType extends AbstractType
{
    /**
     * @var RequestAwarePriceFormatter
     */
    private $priceFormatter;

    public function __construct(RequestAwarePriceFormatter $priceFormatter)
    {
        $this->priceFormatter = $priceFormatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('societyName', TextType::class, [
                'required' => false,
            ])
            ->add('societyTva', TextType::class, [
                'required' => false,
            ])
            ->add('societyLegalForm', TextType::class, [
                'required' => false,
            ])
            ->add('addressStreet', TextType::class, [
                'required' => false,
            ])
            ->add('addressNumber', TextType::class, [
                'required' => false,
            ])
            ->add('addressCity', TextType::class, [
                'required' => false,
            ])
            ->add('addressPostbox', TextType::class, [
                'required' => false,
            ])
            ->add('addressZipCode', TextType::class, [
                'required' => false,
            ])
            ->add('nomContact', TextType::class, [
                'required' => false,
            ])
            ->add('prenomContact', TextType::class, [
                'required' => false,
            ])
            ->add('emailContact', TextType::class, [
                'required' => false,
            ])
            ->add('phoneContact', TextType::class, [
                'required' => false,
            ])
            ->add('pack', EntityType::class, [
                'class'        => Subscription::class,
                'choice_label' => function ($value, $key, $index) {
                    return sprintf('%s (%s HTVA)', $value->getTitle(), $this->priceFormatter->formatPrice($value->getPrice()));
                },
                'label'       => 'become_member.label.pack',
                'placeholder' => 'member.form.pack_placeholder',
                'required'    => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'become_member.label.button',
            ])
            ->add('recaptcha', ReCaptchaType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserMember::class,
        ]);
    }
}
