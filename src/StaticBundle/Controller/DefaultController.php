<?php

namespace StaticBundle\Controller;

use EntityBundle\Entity\User;
use FrontBundle\Controller\BaseController;
use Nietonfir\Google\ReCaptchaBundle\Controller\ReCaptchaValidationInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use StaticBundle\Entity\UserCompany;
use StaticBundle\Entity\UserMember;
use StaticBundle\Entity\UserQuestion;
use StaticBundle\Form\Type\UserCompanyFormType;
use StaticBundle\Form\Type\UserMemberFormType;
use StaticBundle\Form\Type\UserQuestionFormType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController implements ReCaptchaValidationInterface
{
    /**
     * @Route("/", name="static_homepage")
     * @Template("StaticBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("startup", name="static_startup")
     * @Template("StaticBundle:Default:startup.html.twig")
     */
    public function startupAction()
    {
        return [];
    }

    /**
     * @Route("become-member", name="become_member")
     * @Template("StaticBundle:Default:become_member.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function becomeMemberAction(Request $request)
    {
        $userMember = new UserMember();

        $form = $this->createForm(UserMemberFormType::class, $userMember);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->save($userMember);

            $salesUsers = $this->getSalesTeam();
            foreach ($salesUsers as $user) {
                $this->get('front.mail_sender')->send(
                    $user->getEmail(),
                    $this->get('translator')->trans('become_member.email_subject'),
                    'EmailTemplates/new_member_request.html.twig',
                    ['userMember' => $userMember, 'user' => $user]
                );
            }

            return $this->redirectToRoute('become_member_thanks');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("become-member-thanks", name="become_member_thanks")
     * @Template("StaticBundle:Default:become_member_thanks.html.twig")
     */
    public function becomeMemberThankYouAction()
    {
        return [];
    }

    /**
     * @Route("formulaire-company", name="add_user_company")
     * @Template("StaticBundle:Default:user_company.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function userCompanyAction(Request $request)
    {
        $userCompany = new UserCompany();

        $form = $this->createForm(UserCompanyFormType::class, $userCompany);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->save($userCompany);

            $salesUsers = $this->getSalesTeam();
            foreach ($salesUsers as $user) {
                $this->get('front.mail_sender')->send(
                    $user->getEmail(),
                    $this->get('translator')->trans('user_company.email_subject'),
                    'EmailTemplates/new_company_request.html.twig',
                    ['userCompany' => $userCompany, 'user' => $user]
                );
            }

            return $this->redirectToRoute('user_company_thanks');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("user-company-thanks", name="user_company_thanks")
     * @Template("StaticBundle:Default:user_company_thanks.html.twig")
     */
    public function userCompanyThankYouAction()
    {
        return [];
    }

    /**
     * @Route("formulaire-question", name="add_user_question")
     * @Template("StaticBundle:Default:user_question.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function userQuestionAction(Request $request)
    {
        $userQuestion = new UserQuestion();

        $form = $this->createForm(UserQuestionFormType::class, $userQuestion);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->save($userQuestion);

            $salesUsers = $this->getSalesTeam();
            foreach ($salesUsers as $user) {
                $this->get('front.mail_sender')->send(
                    $user->getEmail(),
                    $this->get('translator')->trans('user_question.email_subject'),
                    'EmailTemplates/new_user_question_request.html.twig',
                    ['userQuestion' => $userQuestion, 'user' => $user]
                );
            }

            return $this->redirectToRoute('user_question_thanks');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("user-question-thanks", name="user_question_thanks")
     * @Template("StaticBundle:Default:user_question_thanks.html.twig")
     */
    public function userQuestionThankYouAction()
    {
        return [];
    }

    /**
     * @Route("cookie-policy", name="cookie_policy")
     * @Template("StaticBundle:Default:cookie_policy.html.twig")
     */
    public function cookiePolicyAction()
    {
        return [];
    }

    /**
     * @Route("privacy-policy", name="privacy_policy")
     * @Template("StaticBundle:Default:privacy_policy.html.twig")
     */
    public function privacyPolicyAction()
    {
        return [];
    }

    private function getSalesTeam()
    {
        return $this->getManager()->getRepository(User::class)->getAllSalesTeam();
    }
}
