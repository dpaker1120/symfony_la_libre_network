<?php

namespace AdminBundle\Controller;

use FrontBundle\Entity\Article;
use FrontBundle\Entity\Company;
use FrontBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;

class RaviController extends Controller
{
    /**
     * @Route("cron1", name="cron1")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function cron1xAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $oneMonthLater = new \DateTime('+1 month');
        $companies = $em->getRepository('FrontBundle:Company')->findBy([
            'subscriptionEndDate' => $oneMonthLater,
            'status' => Company::ACTIVE
        ]);

        foreach ($companies as $company) {
            // Send email
            /** @var User $admin */
            $admin = $company->getAdmin();
            $to = $admin->getEmail();
            $subject = "Your La Lebre Network Subscription is going to end in 1 month";
            $description = "Hello " . $admin->getUsername() . ", <br />";
            $description .= "This is gentle reminder that La Lebre Yearly " . $company->getCurrentSubscription()->getTitle() . " Subscription for your company " . $company->getName() . " is going to end in 1 month. ";
            $description .= "Please renew it before expiration to keep enjoying your services.";
            $description .= "Thanks,";
            $description .= "La Lebre Sales Team";
        }

        $twoMonthsLater = new \DateTime('+2 month');
        $companies = $em->getRepository('FrontBundle:Company')->findBy([
            'subscriptionEndDate' => $twoMonthsLater,
            'status' => Company::ACTIVE
        ]);

        foreach ($companies as $company) {
            // Send email
            /** @var User $admin */
            $admin = $company->getAdmin();
            $to = $admin->getEmail();
            $subject = "Your La Lebre Network Subscription is going to end in 2 month";
            $description = "Hello " . $admin->getUsername() . ", <br />";
            $description .= "This is gentle reminder that La Lebre Yearly " . $company->getCurrentSubscription()->getTitle() . " Subscription for your company " . $company->getName() . " is going to end in 2 month. ";
            $description .= "Please renew it before expiration to keep enjoying your services.";
            $description .= "Thanks,";
            $description .= "La Lebre Sales Team";
        }

        echo 'Done';
        die;
    }

    /**
     * @Route("cron2", name="cron2")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function cron2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $companies = $em->getRepository('FrontBundle:Company')->findBy([
            'subscriptionEndDate' => $today,
            'status' => Company::ACTIVE
        ]);

        foreach ($companies as $company) {
            // Set status of company to waiting period
            $company->setState(Company::WAITING_PERIOD);
            $em->persist($companies);

            // Sent email to content writers to unpublish the articles from CMS
            /** @var Article $articles */
            $articles = $em->getRepository('FrontBundle:Company')->find([
                'status' => 'NOT EQUAL TO unpublished'
            ]);

            /** @var Article[] $article */
            foreach ($articles as $article) {
                /** @var User $contentWriter */
                $contentWriter = $article->getContentWriter();

                // Send Email
                $to = $contentWriter->getEmail();
                $subject = 'Please un-publish article "' . $article->getArticleTitle() . '""';
            }

            // Send email
            /** @var User $admin */
            $admin = $company->getAdmin();
        }

        $em->flush();

    }
}
