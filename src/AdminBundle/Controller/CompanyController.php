<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\ProcessExtraCreditFormType;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\CompanySubscriptionHistory;
use EntityBundle\Entity\CreditLog;
use EntityBundle\Entity\Subscription;
use EntityBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("company/")
 */
class CompanyController extends Controller
{
    /**
     * @Route("list", name="company_list")
     * @Security("user.hasAccess('ALL_USER_EXCEPT_LEAD_JOURNALIST')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository(Company::class)->findAll();

        return $this->render('AdminBundle:Company:list.html.twig', [
            'companies' => $companies,
        ]);
    }

    /**
     * @Route("subscription-upgrade/{id}", name="company_subscription_upgrade")
     *
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER')")
     *
     * @Template("AdminBundle:Company:upgrade.html.twig")
     *
     * @return Subscription[]
     */
    public function initUpgradeSubscriptionAction(Company $company)
    {
        $em = $this->getDoctrine()->getManager();

        return [
            'subscriptions' => $em->getRepository(Subscription::class)->findAll(),
            'company'       => $company,
        ];
    }

    /**
     * @Route("subscription/upgrade/{subscription}/{company}", name="upgrade_subscription")
     *
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER')")
     *
     * @param Subscription $subscription
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function upgradeSubscription(Subscription $subscription, Company $company)
    {
        $em = $this->getDoctrine()->getManager();

        $nextSubscriptionStartDate = date('Y-m-d', strtotime('+1 day', strtotime($company->getSubscriptionEndDate()->format('Y-m-d'))));
        $nextSubscriptionEndDate   = date('Y-m-d', strtotime('+1 year', strtotime($nextSubscriptionStartDate)));

        $company
            ->setSubscriptionStartDate(new \DateTime($nextSubscriptionStartDate))
            ->setSubscriptionEndDate(new \DateTime($nextSubscriptionEndDate));

        $company->setCurrentSubscription($subscription);
        $companySubscriptionHistory = CompanySubscriptionHistory::fromCompany($company);
        $this->get('front.credit_log_generator')->createCreditLogs($company, $companySubscriptionHistory);
        Company::renew($company, $subscription);
        $em->persist($companySubscriptionHistory);
        $em->persist($company);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('company.subscription_upgrade'));

        return $this->redirectToRoute('company_list');
    }

    /**
     * @Route("waiting-period-list", name="company_waiting_period_listing")
     * @Security("user.hasAccess('ALL_USER_EXCEPT_LEAD_JOURNALIST')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function waitingPeriodListAction()
    {
        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository(Company::class)->findCompaniesInWaitingPeriod();

        return $this->render('AdminBundle:Company:waiting_period_list.html.twig', [
            'companies' => $companies,
        ]);
    }

    /**
     * @Route("registered-as-dnd/{id}", name="company_register_as_dnd")
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER')")
     *
     * @param Company $company
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function registerCompanyAsDND(Company $company, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($company->isDndRegistered()) {
            $company->setIsDndRegistered(false);
            $successMessage = $this->get('translator')->trans('company_remove_from_dnd');
        } else {
            $company->setIsDndRegistered(true);
            $successMessage = $this->get('translator')->trans('company_registered_as_dnd');
        }

        $em->flush();

        $this->addFlash('success', $successMessage);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("block/{id}", name="block_company")
     *
     * @param Company $company
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER')")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function blockCompany(Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        Company::blocked($company);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('company_blocked'));

        return $this->redirectToRoute('company_waiting_period_listing');
    }

    /**
     * @Route("complete-waiting-period/{id}", name="company_complete_waiting_period")
     * @Security("user.hasAccess('ALL_USER')")
     *
     * @param Company $company
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function completeWaitingPeriodAction(Company $company)
    {
        $em                        = $this->getDoctrine()->getManager();
        $subscription              = $company->getCurrentSubscription();
        $nextSubscriptionStartDate = date('Y-m-d', strtotime('+1 day', strtotime($company->getSubscriptionEndDate()->format('Y-m-d'))));
        $nextSubscriptionEndDate   = date('Y-m-d', strtotime('+1 year', strtotime($nextSubscriptionStartDate)));

        $company
            ->setSubscriptionStartDate(new \DateTime($nextSubscriptionStartDate))
            ->setSubscriptionEndDate(new \DateTime($nextSubscriptionEndDate));

        $companySubscriptionHistory = CompanySubscriptionHistory::fromCompany($company);
        $this->get('front.credit_log_generator')->createCreditLogs($company, $companySubscriptionHistory);
        Company::renew($company, $subscription);

        $em->persist($companySubscriptionHistory);
        $this->enableCompanyUsers($company);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('company_completed_waiting_period'));

        return $this->redirectToRoute('company_waiting_period_listing');
    }

    /**
     * @Route("process-extra-credit/{id}", name="company_process_extra_credit")
     * @Security("user.hasAccess('MANAGER_AND_ACCOUNT_MANAGER')")
     *
     * @param Company $company
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processExtraCreditAction(Company $company, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ProcessExtraCreditFormType::class);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $formData = $form->getData();

            if ($form->get('articleAdd')->isClicked() && null !== $formData['article']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_ARTICLE, $company->getAvailableArticles(), $formData['article']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableArticles(),
                        $company->getAvailableArticles() + $formData['article'],
                        CreditLog::CREDIT_TYPE_ARTICLE,
                        $formData['article']
                    )
                );
                $company->setAvailableArticles($company->getAvailableArticles() + $formData['article']);
            }

            if ($form->get('boostAdd')->isClicked() && null !== $formData['boost']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_BOOST, $company->getAvailableBoosts(), $formData['boost']);

                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableBoosts(),
                        $company->getAvailableBoosts() + $formData['boost'],
                        CreditLog::CREDIT_TYPE_BOOST,
                        $formData['boost']
                    )
                );

                $company->setAvailableBoosts($company->getAvailableBoosts() + $formData['boost']);
            }

            if ($form->get('printAdd')->isClicked() && null !== $formData['print']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_PRINT, $company->getAvailablePrints(), $formData['print']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailablePrints(),
                        $company->getAvailablePrints() + $formData['print'],
                        CreditLog::CREDIT_TYPE_PRINT,
                        $formData['print']
                    )
                );
                $company->setAvailablePrints($company->getAvailablePrints() + $formData['print']);
            }

            if ($form->get('conferenceAdd')->isClicked() && null !== $formData['conference']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_CONFERENCE, $company->getAvailableConferences(), $formData['conference']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableConferences(),
                        $company->getAvailableConferences() + $formData['conference'],
                        CreditLog::CREDIT_TYPE_CONFERENCE,
                        $formData['conference']
                    )
                );
                $company->setAvailableConferences($company->getAvailableConferences() + $formData['conference']);
            }

            if ($form->get('articleRemove')->isClicked() && null !== $formData['article']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_ARTICLE, $company->getAvailableArticles(), $formData['article']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableArticles(),
                        $company->getAvailableArticles() - $formData['article'],
                        CreditLog::CREDIT_TYPE_ARTICLE,
                        $formData['article']
                    )
                );
                $company->setAvailableArticles($company->getAvailableArticles() - $formData['article']);
            }

            if ($form->get('boostRemove')->isClicked() && null !== $formData['boost']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_BOOST, $company->getAvailableBoosts(), $formData['boost']);

                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableBoosts(),
                        $company->getAvailableBoosts() - $formData['boost'],
                        CreditLog::CREDIT_TYPE_BOOST,
                        $formData['boost']
                    )
                );

                $company->setAvailableBoosts($company->getAvailableBoosts() - $formData['boost']);
            }

            if ($form->get('printRemove')->isClicked() && null !== $formData['print']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_PRINT, $company->getAvailablePrints(), $formData['print']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailablePrints(),
                        $company->getAvailablePrints() - $formData['print'],
                        CreditLog::CREDIT_TYPE_PRINT,
                        $formData['print']
                    )
                );
                $company->setAvailablePrints($company->getAvailablePrints() - $formData['print']);
            }

            if ($form->get('conferenceRemove')->isClicked() && null !== $formData['conference']) {
                $this->generateCreditLog(CreditLog::CREDIT_TYPE_CONFERENCE, $company->getAvailableConferences(), $formData['conference']);
                $em->persist(
                    CreditLog::fromExtraCredit(
                        $company,
                        $company->getAvailableConferences(),
                        $company->getAvailableConferences() - $formData['conference'],
                        CreditLog::CREDIT_TYPE_CONFERENCE,
                        $formData['conference']
                    )
                );
                $company->setAvailableConferences($company->getAvailableConferences() - $formData['conference']);
            }

            $em->flush();

            if ($form->get('articleAdd')->isClicked() ||
                $form->get('boostAdd')->isClicked() ||
                $form->get('printAdd')->isClicked() ||
                $form->get('conferenceAdd')->isClicked()
            ) {
                $this->addFlash('success', $this->get('translator')->trans('add_extra_credit.success'));
            } else {
                $this->addFlash('success', $this->get('translator')->trans('remove_extra_credit.success'));
            }

            return $this->redirectToRoute('company_process_extra_credit', ['id' => $company->getId()]);
        }

        return $this->render('AdminBundle:Company:process_extra_credit.html.twig', [
            'form'    => $form->createView(),
            'company' => $company,
        ]);
    }

    /**
     * @Route("view-credit-log/{id}", name="company_view_credit_log")
     * @Security("user.hasAccess('ALL_USER')")
     *
     * @param Company $company
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewCreditLogAction(Company $company)
    {
        $em = $this->getDoctrine()->getManager();

        $logs = $em->getRepository('EntityBundle:CreditLog')->findByCompany($company)->getResult();

        return $this->render('AdminBundle:Company:view_credit_log.html.twig', [
            'logs' => $logs,
        ]);
    }

    /**
     * @param $creditType
     * @param $availableQuantity
     * @param $newQuantity
     */
    private function generateCreditLog($creditType, $availableQuantity, $newQuantity)
    {
        $creditLog = new CreditLog();
        $creditLog
            ->setAmount($newQuantity)
            ->setCreditType($creditType)
            ->setMovementType(CreditLog::MOVEMENT_TYPE_ADD)
            ->setReasonCode(CreditLog::REASON_CODE_PURCHASE_PACK)
            ->setExactReasonLog("Credit has been added for $creditType")
            ->setDate(new \DateTime());

        $this->get('front.credit_log_generator')->maintainSubscriptionBalance(
            CreditLog::MOVEMENT_TYPE_ADD,
            $creditLog,
            $availableQuantity,
            $newQuantity
        );

        $this->getDoctrine()->getManager()->persist($creditLog);
    }

    /**
     * @param Company $company
     */
    private function enableCompanyUsers(Company $company)
    {
        $em = $this->getDoctrine()->getManager();

        $companyUsers = $em->getRepository(User::class)->findBy([
            'company' => $company,
        ]);

        foreach ($companyUsers as $user) {
            $user->setEnabled(true);

            if (in_array('ROLE_CUSTOMER', $user->getRoles(), true)) {
                $this->get('front.mail_sender')
                    ->send($user->getEmail(), $this->get('translator')->trans('company.completed_waiting_period.email_subject'), 'EmailTemplates/company_renewal.html.twig', [
                        'user' => $user,
                    ]);
            }
        }
    }
}
