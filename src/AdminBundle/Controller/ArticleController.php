<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\WriteArticleType;
use EntityBundle\Entity\Article;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\Contact;
use EntityBundle\Entity\CreditLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    /**
     * @Route("un_published_articles", name="un_published_articles")
     * @Security("user.hasAccess('ALL_USER')")
     */
    public function unPublishedArticlesAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->findUnpublishedArticles($company)->getResult();

        return $this->render('AdminBundle:Article:unpublished_listing.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("requested_customer_approval", name="requested_customer_approval")
     * @Security("user.hasAccess('ALL_USER')")
     */
    public function requestedForCustomerApprovalArticlesAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->findForCustomerApprovals($company)->getResult();

        return $this->render('AdminBundle:Article:articles_requested_for_customer_approval.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @param Article $article
     *
     * @Route("show_article/{id}", name="show_article")
     * @Security("user.hasAccess('ALL_USER')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewArticle(Article $article)
    {
        $queAns   = (array) json_decode($article->getQuestions());
        $contacts = $this->getDoctrine()->getEntityManager()->getRepository(Contact::class)->findBy([
            'article' => $article,
        ]);

        return $this->render('AdminBundle:Article:view.html.twig', [
            'article'   => $article,
            'questions' => $queAns,
            'contacts'  => $contacts,
        ]);
    }

    /**
     * @Route("write_article/{id}", name="write_article")
     * @Security("user.hasAccess('CONTENT_WRITER_AND_MANAGER')")
     */
    public function writeArticleAction(Article $article, Request $request)
    {
        $em    = $this->getDoctrine()->getEntityManager();
        $thumb = $article->getArticleThumbnail();

        $form = $this->createForm(WriteArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileManager   = $this->get('admin.file_upload_manager');
            $uploaded_file = $form->get('articleThumbnail')->getData();

            if ($uploaded_file) {
                $fileName = $fileManager->upload($uploaded_file, 'assets/uploads/images');
                $article->setArticleThumbnail('assets/uploads/images/'.$fileName);
            } else {
                $article->setArticleThumbnail($thumb);
            }

            $article->setStatus(Article::ARTICLE_DRAFTED);
            $article->setContentWriter($this->getUser());

            $em->persist($article);
            $em->flush();

            // Email to Admin
            $company = $article->getCompany();
            $admin   = $company->getAdmin();
            $to      = $admin->getEmail();
            $subject = $this->get('translator')->trans('article.your_name').' '.$article->getArticleTitle().' '.$this->get('translator')->trans('article.mail_subject.msg.written');

            $this->get('front.mail_sender')->send($to, $subject, 'EmailTemplates\new_article_written.html.twig', ['article' => $article, 'admin' => $admin]);

            $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.success.article_drafted'));

            return $this->redirectToRoute('un_published_articles');
        }

        return $this->render('@Admin/Article/write_articles.html.twig', [
            'form'    => $form->createView(),
            'article' => $article,
        ]);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("article_change_completed/{id}", name="article_change_completed")
     * @Security("user.hasAccess('CONTENT_WRITER_AND_MANAGER')")
     *
     * @return RedirectResponse
     */
    public function articleChangeCompletedAction(Article $article, Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');
        $article->setStatus(Article::CHANGE_COMPLETED);
        $em->persist($article);
        $em->flush();

        $admin   = $article->getCompany()->getAdmin();
        $to      = $admin->getEmail();
        $subject = $translated->trans('article.msg.change_done').' '.$article->getArticleTitle();

        $mailer->send($to, $subject, 'EmailTemplates/article_change_completed.html.twig', ['article' => $article, 'admin' => $admin]);
        $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.unpublished_articles.success.article_changes_done'));

        // Redirect
        return $this->redirectToRoute('un_published_articles');
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("publish_article/{id}", name="publish_article")
     * @Security("user.hasAccess('LEAD_JOURNALIST_AND_AD_OPS')")
     *
     * @return RedirectResponse
     */
    public function publishArticleAction(Article $article, Request $request)
    {
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $article->setStatus(Article::PUBLISHED);
        $article->setPublishedAt(new \DateTime());

        $em->persist(
            CreditLog::fromArticle($article,
                $article->getCompany()->getAvailableArticles(),
                CreditLog::CREDIT_TYPE_ARTICLE
            )
        );
        $em->persist($article);

        $company = $article->getCompany();
        $company->setAvailableArticles(($company->getAvailableArticles() - 1));
        $em->persist($company);
        $em->flush();

        // Email to Admin
        $admin   = $company->getAdmin();
        $to      = $admin->getEmail();
        $subject = $translated->trans('article.your_name').' '.$article->getArticleTitle().' '.$translated->trans('article.msg.published');

        $mailer->send($to, $subject, 'EmailTemplates\article_published.html.twig', ['article' => $article, 'admin' => $admin]);

        // Set flash msg
        $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.unpublished_articles.success.article_published'));

        // Redirect
        return $this->redirectToRoute('un_published_articles');
    }

    /**
     * @Route("published_articles", name="published_articles")
     * @Security("user.hasAccess('ALL_USER_EXCEPT_LEAD_JOURNALIST')")
     */
    public function publishedArticlesAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->findPublishedArticles($company)->getResult();

        return $this->render('AdminBundle:Article:published_listing.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("articles_requested_for_boost", name="articles_requested_for_boost")
     * @Security("user.hasAccess('ALL_USER')")
     */
    public function articlesRequestedForBoostAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->articlesRequestedForBoost($company)->getResult();

        return $this->render('AdminBundle:Article:articles_requested_for_boost.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("boost_article/{id}", name="boost_article")
     * @Security("user.hasAccess('AD_OPS')")
     *
     * @return RedirectResponse
     */
    public function boostArticleAction(Article $article, Request $request)
    {
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $article->setBoostStatus(Article::ARTICLE_BOOSTED);
        $article->setBoostedAt(new \DateTime());
        $em->persist(
            CreditLog::fromArticle($article,
                $article->getCompany()->getAvailableBoosts(),
                CreditLog::CREDIT_TYPE_BOOST
            )
        );
        $em->persist($article);

        $company = $article->getCompany();
        $company->setAvailableBoosts(($company->getAvailableBoosts() - 1));
        $em->persist($company);
        $em->flush();

        // Email to Admin
        $admin = $company->getAdmin();
        $to    = $admin->getEmail();

        $subject = $translated->trans('article.your_name').' '.$article->getArticleTitle().$translated->trans('boost.request.boosted');

        $mailer->send($to, $subject, 'EmailTemplates/article_boosted.html.twig', ['article' => $article, 'admin' => $admin]);

        // Set flash msg
        $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.articles_requested_for_boost.success.article_boosted'));

        // Redirect
        return $this->redirectToRoute('articles_requested_for_boost');
    }

    /**
     * @Route("articles_requested_for_print", name="articles_requested_for_print")
     * @Security("user.hasAccess('ALL_USER')")
     */
    public function articlesRequestedForPrintAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->articlesRequestedForPrint($company)->getResult();

        return $this->render('AdminBundle:Article:articles_requested_for_print.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("print_article/{id}", name="print_article")
     * @Security("user.hasAccess('AD_OPS')")
     *
     * @return RedirectResponse
     */
    public function printArticleAction(Article $article, Request $request)
    {
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $article->setPrintStatus(Article::ARTICLE_PRINTED);
        $article->setPrintedAt(new \DateTime());
        $em->persist(
            CreditLog::fromArticle($article,
                $article->getCompany()->getAvailablePrints(),
                CreditLog::CREDIT_TYPE_PRINT
            )
        );
        $em->persist($article);

        $company = $article->getCompany();
        $company->setAvailablePrints(($company->getAvailablePrints() - 1));
        $em->persist($company);
        $em->flush();

        // Email to Admin
        $admin   = $company->getAdmin();
        $to      = $admin->getEmail();
        $subject = $translated->trans('article.your_name').' '.$article->getArticleTitle().$translated->trans('print.request.printed');

        $mailer->send($to, $subject, 'EmailTemplates/article_printed.html.twig', ['article' => $article, 'admin' => $admin]);

        // Set flash msg
        $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.articles_requested_for_print.success.article_printed'));

        // Redirect
        return $this->redirectToRoute('articles_requested_for_print');
    }

    /**
     * @Route("articles_requested_for_unpublishing", name="articles_requested_for_unpublishing")
     * @Security("user.hasAccess('ALL_USER')")
     */
    public function articlesRequestedForUnpublishingAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $company   = null;
        $companyId = $request->query->get('id', null);
        if ($companyId !== null) {
            $company = $em->getRepository('EntityBundle:Company')->find($companyId);
        }

        $articles = $em->getRepository('EntityBundle:Article')->articlesRequestedForUnPublishing($company)->getResult();

        return $this->render('AdminBundle:Article:articles_requested_for_unPublishing.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("unpublish_article/{id}", name="unpublish_article")
     * @Security("user.hasAccess('LEAD_JOURNALIST_AND_AD_OPS')")
     *
     * @return RedirectResponse
     */
    public function unpublishArticleAction(Article $article, Request $request)
    {
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        if ($article->getStatus() == Article::UNPUBLISHED_REQUESTED_AUTO) {
            $article->setStatus(Article::UNPUBLISHED_AUTO);
        } else {
            $article->setStatus(Article::UNPUBLISHED);
        }
        $em->persist($article);
        $em->flush();

        // Email to Admin
        $company = $article->getCompany();
        $admin   = $company->getAdmin();
        $to      = $admin->getEmail();
        $subject = $translated->trans('article.your_name').' '.$article->getArticleTitle().$translated->trans('article.msg.unpublished');

        $mailer->send($to, $subject, 'EmailTemplates/article_unpublish.html.twig', ['article' => $article, 'admin' => $admin]);

        // Set flash msg
        $this->addFlash('success', $this->get('translator')->trans('page.listing.backoffice.articles_requested_for_unpublishing.success.article_unpublished'));

        // Redirect
        return $this->redirectToRoute('articles_requested_for_unpublishing');
    }
}
