<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\CustomerEditFormType;
use AdminBundle\Form\Type\CustomerFormType;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\CompanySubscriptionHistory;
use EntityBundle\Entity\User;
use FOS\UserBundle\Model\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("customer/")
 */
class CustomerController extends Controller
{
    /**
     * @Route("add", name="customer_add")
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER_AND_ACCOUNT_MANAGER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $company = new Company();
        $company->addCompanyRepresentative(new User());

        $form = $this->createForm(CustomerFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->createCustomerAdmin($form, $company);
            $this->createRepresentative($company);
            $subscription = $company->getCurrentSubscription();
            $companySubscriptionHistory = CompanySubscriptionHistory::fromCompany($company);
            $this->get('front.credit_log_generator')->createCreditLogs($company, $companySubscriptionHistory);
            Company::renew($company, $subscription);

            $em->persist($companySubscriptionHistory);
            $em->persist($company);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('customer_created'));

            return $this->redirectToRoute('company_list');
        }

        return $this->render('AdminBundle:Customer:add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("edit/{id}", name="customer_edit")
     * @Security("user.hasAccess('SALES_TEAM_AND_MANAGER_AND_ACCOUNT_MANAGER')")
     *
     * @param Company $company
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Company $company, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CustomerEditFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($company);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('customer_edited'));

            return $this->redirectToRoute('company_list');
        }

        return $this->render('AdminBundle:Customer:edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Company $company
     */
    private function createRepresentative(Company $company)
    {
        $representatives = $company->getCompanyRepresentatives();

        $userManager = $this->get('fos_user.user_manager');

        foreach ($representatives as $representative) {
            if (null == $representative->getEmail() || null == $representative->getName() || null == $representative->getJobTitle()) {
                continue;
            }

            $user     = $userManager->createUser();
            $password = $this->generatePassword();

            $user
                ->setEmail($representative->getEmail())
                ->setName($representative->getName())
                ->setEnabled(true)
                ->setJobTitle($representative->getJobTitle())
                ->setUsername($representative->getEmail())
                ->setPlainPassword($password)
                ->addRole('ROLE_REPRESENTATIVE')
                ->setCompany($company);

            $userManager->updateUser($user, false);

            $company->addUser($user);

            $this->sendMailToUser($user, $password);
        }
    }

    private function createCustomerAdmin(Form $form, Company $company)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user        = $userManager->createUser();
        $password    = $this->generatePassword();

        $user
            ->setEmail($form->get('adminEmail')->getData())
            ->setName($form->get('adminName')->getData())
            ->setEnabled(true)
            ->setJobTitle($form->get('adminJobTitle')->getData())
            ->setUsername($form->get('adminEmail')->getData())
            ->setPlainPassword($password)
            ->addRole('ROLE_CUSTOMER')
            ->setCompany($company);

        $userManager->updateUser($user, false);
        $company->addUser($user);

        $this->sendMailToUser($user, $password);
    }

    /**
     * @param User   $user
     * @param string $password
     */
    private function sendMailToUser(User $user, $password)
    {
        $this->get('front.mail_sender')
            ->send($user->getEmail(), $this->get('translator')->trans('company.new_user.email_subject'), 'EmailTemplates/new_representative.html.twig', [
                'user'     => $user,
                'password' => $password,
            ]);
    }

    private function generatePassword()
    {
        $tokenGenerator = $this->get('fos_user.util.token_generator');

        return substr($tokenGenerator->generateToken(), 0, 12);
    }
}
