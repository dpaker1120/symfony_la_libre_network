<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\UserFormType;
use EntityBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("user/")
 */
class UserController extends Controller
{
    /**
     * @Route("add", name="user_add")
     * @Template("AdminBundle:User:add.html.twig")
     * @Security("user.hasAccess('ADMIN')")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        $user = new User();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->generatePassword();
            $user
                ->setUsername($user->getEmail())
                ->setEnabled(true)
                ->setRoles($form->get('role')->getData())
                ->setPlainPassword($password);

            $this->get('front.mail_sender')
                ->send($user->getEmail(), $this->get('translator')->trans('user.new_user.email_subject'), 'EmailTemplates/new_user.html.twig', [
                    'user'     => $user,
                    'password' => $password,
                    'role'     => $form->get('role')->getData(),
                ]);

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('user.add.success'));

            return $this->redirectToRoute('user_list');
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @Route("list", name="user_list")
     * @Security("user.hasAccess('ADMIN')")
     * @Template("AdminBundle:User:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->getUsers();

        return [
            'users' => $users,
        ];
    }

    /**
     * @Route("edit/{id}", name="user_edit")
     * @Template("AdminBundle:User:add.html.twig")
     *
     * @param User    $user
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles($form->get('role')->getData());

            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('user.edit.success'));

            return $this->redirectToRoute('user_list');
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @Route("change-status/{id}", name="user_change_status")
     *
     * @param User $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeStatusAction(User $user)
    {
        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $message = 'user.disable_success';
        } else {
            $user->setEnabled(true);
            $message = 'user.enable_success';
        }

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', $this->get('translator')->trans($message));

        return $this->redirectToRoute('user_list');
    }

    private function generatePassword()
    {
        $tokenGenerator = $this->get('fos_user.util.token_generator');

        return substr($tokenGenerator->generateToken(), 0, 12);
    }
}
