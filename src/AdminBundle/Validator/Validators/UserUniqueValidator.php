<?php

namespace AdminBundle\Validator\Validators;

use Doctrine\ORM\EntityManager;
use EntityBundle\Entity\User;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserUniqueValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string     $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $value]);

        if (null !== $user) {
            $this->context->buildViolation('user.email.unique')
                ->addViolation();
        }
    }
}
