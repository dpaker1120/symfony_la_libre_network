<?php

namespace AdminBundle\Form\Type;

use EntityBundle\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerEditFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'company.label.name'
            ])
            ->add('addressLine1', TextType::class, [
                'label' => 'company.label.address1'
            ])
            ->add('addressLine2', TextType::class, [
                'label' => 'company.label.address2'
            ])
            ->add('subscriptionStartDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'label' => 'company.label.start_date'
            ])
            ->add('subscriptionEndDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'label' => 'company.label.end_date'
            ])
            ->add('vat', TextType::class, [
                'label' => 'company.label.vat'
            ])
            ->add('phone', TextType::class, [
                'label' => 'company.label.phone'
            ])
            ->add('city', TextType::class, [
                'label' => 'company.label.city'
            ])
           /* ->add('state', TextType::class, [
                'label' => 'company.label.state'
            ])*/
            ->add('country', TextType::class, [
                'label' => 'company.label.country'
            ])
            ->add('zip', TextType::class, [
                'label' => 'company.label.zip'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'company.submit'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}