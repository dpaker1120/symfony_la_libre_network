<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WriteArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shortDescription', TextareaType::class, [
                'label' => 'label.shortdescription',
                'required' => true,
            ])
            ->add('externalCmsLink', UrlType::class, [
                'label'    => 'label.cms_link',
                'required' => true,
            ])
            ->add('articleThumbnail', FileType::class, [
                'label' => 'label.image',
                'data_class' => null,
                'required' => false,
            ])
            ->add('articleTitle', TextType::class, [
                'label'    => 'label.article.label',
                'required' => true,
            ])
            ->add('submit', SubmitType::class, ['label' => 'btn.submit']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'EntityBundle\Entity\Article',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'frontbundle_article';
    }
}
