<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ProcessExtraCreditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('article', IntegerType::class, [
                'label'    => 'label.credit_article',
                'required' => false,
                'attr'     => [
                    'min' => 0,
                ],
            ])
            ->add('boost', IntegerType::class, [
                'label'    => 'label.credit_boost',
                'required' => false,
                'attr'     => [
                    'min' => 0,
                ],
            ])
            ->add('print', IntegerType::class, [
                'label'    => 'label.credit_print',
                'required' => false,
                'attr'     => [
                    'min' => 0,
                ],
            ])
            ->add('conference', IntegerType::class, [
                'label'    => 'label.credit_conference',
                'required' => false,
                'attr'     => [
                    'min' => 0,
                ],
            ])
            ->add('articleAdd', SubmitType::class, [
                'label' => 'label.credit_article.add',
            ])
            ->add('boostAdd', SubmitType::class, [
                'label' => 'label.credit_boost.add',
            ])
            ->add('printAdd', SubmitType::class, [
                'label' => 'label.credit_print.add',
            ])
            ->add('conferenceAdd', SubmitType::class, [
                'label' => 'label.credit_conference.add',
            ])
            ->add('articleRemove', SubmitType::class, [
                'label' => 'label.credit_remove',
            ])
            ->add('boostRemove', SubmitType::class, [
                'label' => 'label.credit_remove',
            ])
            ->add('printRemove', SubmitType::class, [
                'label' => 'label.credit_remove',
            ])
            ->add('conferenceRemove', SubmitType::class, [
                'label' => 'label.credit_remove',
            ]);
    }
}
