<?php

namespace AdminBundle\Form\Type;

use EntityBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['data'];
        $builder
            ->add('name', TextType::class, [
                'label' => 'user.label.name',
            ])
            ->add('jobTitle', TextType::class, [
                'label' => 'user.label.job_title',
            ])
            ->add('email', EmailType::class, [
                'label' => 'user.label.email',
            ])
            ->add('role', ChoiceType::class, [
                'placeholder' => 'user.label.role_placeholder',
                'multiple' => true,
                'required'    => false,
                'label'       => 'user.label.role',
                'mapped'      => false,
                'choices'     => $this->getRoles(),
                'data'        => $user->getRoles(),
                'constraints' => new NotNull(['groups' => 'user_create', 'message' => 'user.role.required']),
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'user.label.submit',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class'        => User::class,
                'validation_groups' => 'user_create',
            ]);
    }

    private function getRoles()
    {
        return [
            'Content Writer'  => 'ROLE_CONTENT_WRITER',
            'Sales Team'      => 'SALES_TEAM',
            'Ad ops'          => 'AD_OPS',
            'Lead Journalist' => 'ROLE_LEAD_JOURNALIST',
            'Manager'         => 'ROLE_MANAGER',
            'Account Manager' => 'ROLE_ACCOUNT_MANAGER',
        ];
    }
}
