<?php

namespace AdminBundle\Form\Type;

use EntityBundle\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepresentativeFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'    => 'company.label.representative_name',
                'required' => false,
            ])
            ->add('email', TextType::class, [
                'label'    => 'company.label.representative_email',
                'required' => false,
            ])
            ->add('jobTitle', TextType::class, [
                'label'    => 'company.label.representative_job_title',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'  => User::class,
            'constraints' => [
                new UniqueEntity(['fields' => ['email']]),
            ],
        ]);
    }
}
