<?php

namespace AdminBundle\Form\Type;

use AdminBundle\Validator\Constraints\UserUniqueConstraint;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\Subscription;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class CustomerFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'company.label.name'
            ])
            ->add('addressLine1', TextType::class, [
                'label' => 'company.label.address1'
            ])
            ->add('addressLine2', TextType::class, [
                'label' => 'company.label.address2'
            ])
            ->add('currentSubscription', EntityType::class, [
                'class'        => Subscription::class,
                'choice_label' => 'title',
                'label' => 'company.label.subscription'
            ])
            ->add('subscriptionStartDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'label' => 'company.label.start_date'
            ])
            ->add('subscriptionEndDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'label' => 'company.label.end_date'
            ])
            ->add('vat', TextType::class, [
                'label' => 'company.label.vat'
            ])
            ->add('phone', TextType::class, [
                'label' => 'company.label.phone'
            ])
            ->add('city', TextType::class, [
                'label' => 'company.label.city'
            ])
           /* ->add('state', TextType::class, [
                'label' => 'company.label.state'
            ])*/
            ->add('country', TextType::class, [
                'label' => 'company.label.country'
            ])
            ->add('zip', TextType::class, [
                'label' => 'company.label.zip'
            ])
            ->add('adminName', TextType::class, [
                'mapped'      => false,
                'label' => 'company.label.admin_name',
                'constraints' => new NotNull(['message' => 'user_name.required']),
            ])
            ->add('adminEmail', TextType::class, [
                'mapped'      => false,
                'label' => 'company.label.admin_email',
                'constraints' => [
                    new NotNull(['message' => 'user_email.required']),
                    new UserUniqueConstraint()
                ],
            ])
            ->add('adminJobTitle', TextType::class, [
                'mapped'      => false,
                'label' => 'company.label.admin_job_title',
                'constraints' => new NotNull(['message' => 'user_job_title.required']),
            ])
            ->add('companyRepresentatives', CollectionType::class, [
                'label' => 'company.label.representatives',
                'entry_type'     => RepresentativeFormType::class,
                'allow_add'      => true,
                'allow_delete'   => true,
                'error_bubbling' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'company.submit'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
