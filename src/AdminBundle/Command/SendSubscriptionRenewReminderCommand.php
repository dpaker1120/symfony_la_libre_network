<?php

namespace AdminBundle\Command;

use EntityBundle\Entity\Company;
use EntityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendSubscriptionRenewReminderCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('companies:subscription:reminder:for_renewal')
            ->setDescription('Send subscription renew reminder')
            ->setHelp("Get all the customers whose subscription is about to end.\nSend email to the customer whose subscription will end after 1 or 2 month");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('request.host'));
        $context->setScheme($this->getContainer()->getParameter('request.scheme'));

        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $translator = $this->getContainer()->get('translator');
        $companies  = $em->getRepository(Company::class)->getCustomersWhoseSubscriptionIsNearToEnd();
        $mailSender = $this->getContainer()->get('front.mail_sender');
        foreach ($companies as $company) {
            $timeDiff = $company->getSubscriptionEndDate()->diff(new \DateTime());
            $this->informSalesTeam($company, $output, $timeDiff);
            $mailSender->send(
                $company->getAdmin()->getEmail(),
                $translator->trans('subscription.renew'),
                $timeDiff->days > 30 ? 'AdminBundle:EmailTemplates:subscription_renewal_for_2_month_reminder.html.twig' : 'AdminBundle:EmailTemplates:subscription_renewal_for_1_month_reminder.html.twig',
                ['company' => $company]
            );

            $output->write('Send email to the '.$company->getName()."\n");
        }
    }

    /**
     * @param Company         $company
     * @param OutputInterface $output
     */
    private function informSalesTeam(Company $company, OutputInterface $output, $timeDiff)
    {
        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mailSender = $this->getContainer()->get('front.mail_sender');
        $translator = $this->getContainer()->get('translator');
        $mailSender->send(
            $em->getRepository(User::class)->getSalesPerson()->getEmail(),
            $translator->trans('subscription.renew') .$company->getName(),
            $timeDiff->days > 30 ? 'AdminBundle:EmailTemplates:subscription_renew_for_2_inform_sales_team.html.twig' : 'AdminBundle:EmailTemplates:subscription_renew_for_1_inform_sales_team.html.twig',
            ['company' => $company, 'sales_user' => $em->getRepository(User::class)->getSalesPerson()]
        );

        $output->writeln('Notify sales team.');
    }
}
