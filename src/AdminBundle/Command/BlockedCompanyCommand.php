<?php

namespace AdminBundle\Command;

use EntityBundle\Entity\Article;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BlockedCompanyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('companies:block')
            ->setDescription('Mark company as waiting')
            ->setHelp("Mark company as waiting if company's subscription is ended by today.");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('request.host'));
        $context->setScheme($this->getContainer()->getParameter('request.scheme'));

        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mailSender = $this->getContainer()->get('front.mail_sender');
        $translator = $this->getContainer()->get('translator');
        $companies  = $em->getRepository(Company::class)->getCustomersWhoseSubscriptionIsAlreadyEndedBeforeTwoMonths();

        foreach ($companies as $company) {
            $this->notifyContentWriters($company, $output);
            $this->notifySalesTeam($company, $output);
            $this->markArticlesAsUnpublished($company);
            if (false === $company->isDndRegistered()) {
                $mailSender->send(
                    $company->getAdmin()->getEmail(),
                    $translator->trans('your.account.block'),
                    'AdminBundle:EmailTemplates:company_block_notify_customer.html.twig',
                    ['company' => $company]
                );
            }

            $em->persist(Company::blocked($company));
            $output->write('Blocked '.$company->getName()."\n");
        }

        $em->flush();
    }

    /**
     * @param Company         $company
     * @param OutputInterface $output
     */
    private function notifyContentWriters(Company $company, OutputInterface $output)
    {
        $mailSender            = $this->getContainer()->get('front.mail_sender');
        $contentWritersDetails = [];
        foreach ($company->getArticles() as $article) {
            if ($article->getDevOp()) {
                $contentWritersDetails[$article->getDevOp()->getEmail()]['articles'][] = [
                    'name' => $article->getArticleTitle(),
                ];
                $contentWritersDetails[$article->getDevOp()->getEmail()]['name'] = $article->getDevOp()->getName();
            }
        }

        foreach ($contentWritersDetails as $email => $contentWritersDetail) {
            $mailSender->send(
                $email,
                'Unpublished articles for '.$company->getName().'',
                'AdminBundle:EmailTemplates:company_block_notify_content_writer.html.twig',
                ['details' => $contentWritersDetail, 'content_writer_name' => $contentWritersDetail['name'], 'company' => $company]
            );
            $output->writeln('Notify content writer ('.$contentWritersDetail['name'].') to remove article for '.$company->getName());
        }
    }

    /**
     * @param Company         $company
     * @param OutputInterface $output
     */
    private function notifySalesTeam(Company $company, OutputInterface $output)
    {
        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mailSender = $this->getContainer()->get('front.mail_sender');
        $mailSender->send(
            $em->getRepository(User::class)->getSalesPerson()->getEmail(),
            'Subscription End Today for '.$company->getName(),
            'AdminBundle:EmailTemplates:company_block_notify_sales_team.html.twig',
            ['company' => $company, 'sales_user' => $em->getRepository(User::class)->getSalesPerson()]
        );

        $output->writeln('Notify sales team.');
    }

    /**
     * @param Company $company
     */
    private function markArticlesAsUnpublished(Company $company)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        foreach ($company->getArticles() as $article) {
            $article->setStatus(Article::UNPUBLISHED_REQUESTED_AUTO);
            $em->persist($article);
        }
    }
}
