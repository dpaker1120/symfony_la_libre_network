<?php

namespace AdminBundle\Command;

use EntityBundle\Entity\Company;
use EntityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddCompanyInWaitingPeriodCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('companies:end_subscription')
            ->setDescription('Mark company as waiting')
            ->setHelp("Mark company as waiting if company's subscription is ended by today.\nThis command will send emails to the customer and sales team to inform about subscription end.");

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('request.host'));
        $context->setScheme($this->getContainer()->getParameter('request.scheme'));

        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mailSender = $this->getContainer()->get('front.mail_sender');
        $companies  = $em->getRepository(Company::class)->getCustomerWhoseSubscriptionIsEndsToday();
        $translator = $this->getContainer()->get('translator');
        foreach ($companies as $company) {
            $this->informSalesTeam($company, $output);
            if (false === $company->isDndRegistered()) {
                $mailSender->send(
                    $company->getAdmin()->getEmail(),
                    $translator->trans('company.subscription_end_msg'),
                    'AdminBundle:EmailTemplates:subscription_end.html.twig',
                    ['company' => $company, 'renew_last_date' => new \DateTime('+2 month')]
                );
            }

            $company->setStatus(Company::WAITING_PERIOD);
            $em->persist($company);

            $output->write('Mark status as waiting period for '.$company->getName()."\n");
        }

        $em->flush();
    }

    /**
     * @param Company         $company
     * @param OutputInterface $output
     */
    private function informSalesTeam(Company $company, OutputInterface $output)
    {
        $em         = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mailSender = $this->getContainer()->get('front.mail_sender');
        $translator = $this->getContainer()->get('translator');
        $mailSender->send(
            $em->getRepository(User::class)->getSalesPerson()->getEmail(),
            $translator->trans('company.subscription_end_msg') .$company->getName(),
            'AdminBundle:EmailTemplates:subscription_end_inform_sales_team.html.twig',
            ['company' => $company, 'sales_user' => $em->getRepository(User::class)->getSalesPerson()]
        );

        $output->writeln('Notify sales team.');
    }
}
