<?php


namespace AdminBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadManager
{
    /**
     * @param UploadedFile $file
     * @param string       $uploadPath
     *
     * @return string
     */
    public function upload(UploadedFile $file, $uploadPath)
    {
        $fileName = $this->generateFileName($file);

        $file->move($uploadPath, $fileName);

        return $fileName;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function generateFileName(UploadedFile $file)
    {
        return rtrim($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).'_'.md5(uniqid()).'.'.$file->getClientOriginalExtension();
    }
}