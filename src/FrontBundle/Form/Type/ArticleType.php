<?php

namespace FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enterpriseName', TextType::class , [
                'label' => 'label.enterpriseName',
                'required' => false,
            ] )
            ->add('turnover', IntegerType::class, [
                'label' => 'label.turnover',
                'required' => false,
            ] )
            ->add('numberOfEmployees', IntegerType::class, [
                'label' => 'label.numberOfEmployees',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, ['label' => 'btn.submit']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'EntityBundle\Entity\Article',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'frontbundle_article';
    }
}
