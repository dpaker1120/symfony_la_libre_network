<?php

namespace FrontBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginListener implements EventSubscriberInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        ];
    }

    /**
     * Do the magic.redirect function.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user         = $event->getAuthenticationToken()->getUser();
        $allowedRoles = $user->getRoles();
        $request      = $event->getRequest();

        if (in_array('ROLE_ADMIN', $allowedRoles)) {
            $targetPath = $this->generateAbsoluteUrl('user_list');
        } elseif (in_array('ROLE_CONTENT_WRITER', $allowedRoles) ||
            in_array('SALES_TEAM', $allowedRoles) ||
            in_array('AD_OPS', $allowedRoles) ||
            in_array('ROLE_LEAD_JOURNALIST', $allowedRoles) ||
            in_array('ROLE_MANAGER', $allowedRoles) ||
            in_array('ROLE_ACCOUNT_MANAGER', $allowedRoles)
        ) {
            $targetPath = $this->generateAbsoluteUrl('un_published_articles');
        } elseif (in_array('ROLE_CUSTOMER', $allowedRoles) || in_array('ROLE_REPRESENTATIVE', $allowedRoles)) {
            $targetPath = $this->generateAbsoluteUrl('article_list');
        }
        $request->request->set('_target_path', $targetPath);
    }

    /**
     * @param string $route
     * @param array  $params
     *
     * @return string
     */
    private function generateAbsoluteUrl($route, array $params = [])
    {
        return $this->router->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
