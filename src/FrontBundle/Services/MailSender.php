<?php

namespace FrontBundle\Services;

use Swift_Mailer;
use Swift_Message;
use Twig_Environment;

class MailSender
{
    /** @var Swift_Message */
    private $mailer;

    /** @var Twig_Environment */
    private $twig;

    /** @var string */
    private $fromEmail;

    public function __construct(Swift_Mailer $mailer, Twig_Environment $twig, $fromEmail)
    {
        $this->mailer    = $mailer;
        $this->twig      = $twig;
        $this->fromEmail = $fromEmail;
    }

    /**
     * Prepare message to sent.
     *
     * @param string $to
     * @param string $subject
     * @param string $twig
     * @param array  $parameters
     *
     * @return bool
     */
    private function prepare($to, $subject, $twig, array $parameters)
    {
        return  Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmail)
            ->setTo($to)
            ->setBody(
                $this->twig->render($twig, $parameters),
                'text/html'
            );
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $twig
     * @param array  $parameters
     *
     * @return bool
     */
    public function send($to, $subject, $twig, array $parameters)
    {
        $this->mailer->send($this->prepare($to, $subject, $twig, $parameters));

        return true;
    }
}
