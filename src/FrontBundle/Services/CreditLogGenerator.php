<?php

namespace FrontBundle\Services;

use Doctrine\ORM\EntityManager;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\CompanySubscriptionHistory;
use EntityBundle\Entity\CreditLog;
use Symfony\Component\Translation\TranslatorInterface;

class CreditLogGenerator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityManager $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator    = $translator;
    }

    /**
     * @param Company                    $company
     * @param CompanySubscriptionHistory $companySubscriptionHistory
     * @param string                     $movementType
     */
    public function createCreditLogs(
        Company $company,
        CompanySubscriptionHistory $companySubscriptionHistory,
        $movementType = CreditLog::MOVEMENT_TYPE_ADD
    ) {
        $subscription = $company->getCurrentSubscription();
        if ($subscription->getTotalArticles() > 0) {
            $companyLog = new CreditLog();
            $companyLog
                ->setCompanySubscriptionHistory($companySubscriptionHistory)
                ->setCreditType(CreditLog::CREDIT_TYPE_ARTICLE)
                ->setDate(new \DateTime())
                ->setAmount($subscription->getTotalArticles())
                ->setOldBalance($company->getAvailableArticles())
                ->setMovementType(CreditLog::MOVEMENT_TYPE_ADD)
                ->setReasonCode(CreditLog::REASON_CODE_PURCHASE_PACK)
                ->setExactReasonLog($this->translator->trans('creditlog.msg.added'));

            $this->maintainSubscriptionBalance(
                $movementType,
                $companyLog,
                $company->getAvailableArticles(),
                $subscription->getTotalArticles()
            );

            $this->entityManager->persist($companyLog);
        }

        if ($subscription->getTotalBoosts() > 0) {
            $companyLog = new CreditLog();
            $companyLog
                ->setCompanySubscriptionHistory($companySubscriptionHistory)
                ->setCreditType(CreditLog::CREDIT_TYPE_BOOST)
                ->setDate(new \DateTime())
                ->setAmount($subscription->getTotalBoosts())
                ->setMovementType(CreditLog::MOVEMENT_TYPE_ADD)
                ->setReasonCode(CreditLog::REASON_CODE_PURCHASE_PACK)
                ->setExactReasonLog($this->translator->trans('creditlog.msg.added'));

            $this->maintainSubscriptionBalance(
                $movementType,
                $companyLog,
                $company->getAvailableBoosts(),
                $subscription->getTotalBoosts()
            );

            $this->entityManager->persist($companyLog);
        }

        if ($subscription->getTotalPrints() > 0) {
            $companyLog = new CreditLog();
            $companyLog
                ->setCompanySubscriptionHistory($companySubscriptionHistory)
                ->setCreditType(CreditLog::CREDIT_TYPE_PRINT)
                ->setDate(new \DateTime())
                ->setAmount($subscription->getTotalPrints())
                ->setMovementType(CreditLog::MOVEMENT_TYPE_ADD)
                ->setReasonCode(CreditLog::REASON_CODE_PURCHASE_PACK)
                ->setExactReasonLog($this->translator->trans('creditlog.msg.added'));

            $this->maintainSubscriptionBalance(
                $movementType,
                $companyLog,
                $company->getAvailablePrints(),
                $subscription->getTotalPrints()
            );

            $this->entityManager->persist($companyLog);
        }

        if ($subscription->getTotalConferences() > 0) {
            $companyLog = new CreditLog();
            $companyLog
                ->setCompanySubscriptionHistory($companySubscriptionHistory)
                ->setCreditType(CreditLog::CREDIT_TYPE_CONFERENCE)
                ->setDate(new \DateTime())
                ->setAmount($subscription->getTotalConferences())
                ->setMovementType(CreditLog::MOVEMENT_TYPE_ADD)
                ->setReasonCode(CreditLog::REASON_CODE_PURCHASE_PACK)
                ->setExactReasonLog($this->translator->trans('creditlog.msg.added'));

            $this->maintainSubscriptionBalance(
                $movementType,
                $companyLog,
                $company->getAvailableConferences(),
                $subscription->getTotalConferences()
            );

            $this->entityManager->persist($companyLog);
        }
    }

    /**
     * @param string    $movementType
     * @param CreditLog $companyLog
     * @param int       $oldBalance
     * @param int       $subscriptionBalance
     */
    public function maintainSubscriptionBalance(
        $movementType,
        CreditLog $companyLog,
        $oldBalance,
        $subscriptionBalance
    ) {
        $companyLog->setOldBalance($oldBalance);
        if ($movementType == CreditLog::MOVEMENT_TYPE_ADD) {
            $companyLog->setNewBalance($oldBalance + $subscriptionBalance);
        }

        if ($movementType == CreditLog::MOVEMENT_TYPE_CONSUME) {
            $companyLog->setNewBalance($oldBalance - 1);
        }

        if ($movementType == CreditLog::MOVEMENT_TYPE_CORRECTION) {
            $companyLog->setNewBalance($oldBalance + 1);
        }
    }
}
