<?php

namespace FrontBundle\Controller;

use EntityBundle\Entity\Article;
use EntityBundle\Entity\Company;
use EntityBundle\Entity\Contact;
use EntityBundle\Entity\User;
use FrontBundle\Form\Type\ArticleType;
use FrontBundle\Form\Type\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/", name="article_list")
     * @Security("has_role('ROLE_REPRESENTATIVE')")
     */
    public function indexAction()
    {
        $em       = $this->getDoctrine()->getEntityManager();
        $user     = $this->getUser();
        $company = $user->getCompany();
        $articles = $em->getRepository('EntityBundle:Article')->findBy(['company' => $user->getCompany()], ['id' => 'DESC']);

        return $this->render('@Front/article/list.html.twig', [
            'articles' => $articles,
            'company' => $company
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/add", name="article_add")
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();

        if (!$company->isSubscriptionRunning()) {
            $this->addFlash('error', $this->get('translator')->trans('company.subscription_not_started'));

            return $this->redirectToRoute('article_list');
        }

        if ($company->getAvailableArticles() == 0) {
            $this->addFlash('error', $this->get('translator')->trans('company.article.limit.reached'));

            return $this->redirectToRoute('article_list');
        }

        if ($company->getStatus() != Company::ACTIVE) {
            $this->addFlash('error', $this->get('translator')->trans('company.article.not_active'));

            return $this->redirectToRoute('article_list');
        }

        $article        = new Article();
        $questionsArray = $this->container->getParameter('questions');
        $em             = $this->getDoctrine()->getEntityManager();
        $form           = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        $errorMessage = null;

        if ($form->isSubmitted()) {
            $translated = $this->get('translator');
            $questions  = $request->request->get('question');
            $answers    = $request->request->get('answer');
            $queAns     = array_combine($questions, $answers);

            foreach ($queAns as $k => $v) {
                if (empty($v)) {
                    $errorMessage = $translated->trans('question.required');
                }
            }

            if ($form->isValid() && $errorMessage == null) {
                $article->setQuestions(json_encode($queAns));
                $article->setUser($this->getUser());
                $article->setStatus(Article::ARTICLE_REQUESTED);
                $article->setCompany($this->getUser()->getCompany());
                $article->setDevOp($em->getRepository(User::class)->findRandomAdOps());

                $em->persist($article);
                $em->flush();

                $this->addFlash('success', $this->get('translator')->trans('article.create_success'));

                return $this->redirectToRoute('article_list');
            }
        }

        return $this->render('@Front/article/new.html.twig', [
            'form'      => $form->createView(),
            'questions' => $questionsArray,
            'error_msg' => $errorMessage,
        ]);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("/view_article/{id}", name="view_article")
     * @Security("has_role('ROLE_REPRESENTATIVE')")
     *
     * @return Response
     */
    public function viewArticleAction(Article $article, Request $request)
    {
        $contact = new Contact();
        $form    = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        return $this->render('@Front/article/view_drafted_article.html.twig', [
            'article' => $article,
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Article $article
     *
     * @Route("/request_article_change/{id}", name="request_article_change")
     * @Security("has_role('ROLE_CUSTOMER')")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function requestArticleChangeAction(Request $request, Article $article)
    {
        $em         = $this->getDoctrine()->getEntityManager();
        $translated = $this->get('translator');
        $message    = $request->request->get('message');

        if ($message) {
            $contact = new Contact();
            $contact->setDescription($message);
            $contact->setArticle($article);
            $article->setStatus(Article::CHANGE_REQUESTED);
            $em->persist($contact);
            $em->persist($article);
            $em->flush();

            $mailer = $this->get('front.mail_sender');
            $mailer->send($article->getContentWriter()->getEmail(), $translated->trans('label.customer').' '.$article->getCompany()->getName().' '.$translated->trans('subject.contact'), 'EmailTemplates\contact.html.twig', ['article' => $article]);

            $this->addFlash('success', $this->get('translator')->trans('customer.article.requested_sent.message'));

            return new JsonResponse(['data' => $message]);
        }

        return new Response('Error!', 400);
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("/approve_article/{id}", name="approve_article")
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return Response
     */
    public function approveArticleAction(Article $article, Request $request)
    {
        $em         = $this->getDoctrine()->getEntityManager();
        $mailer     = $this->get('front.mail_sender');
        $translated = $this->get('translator');

        if ($article->getStatus() == Article::ARTICLE_DRAFTED || $article->getStatus() == Article::CHANGE_COMPLETED) {
            $article->setStatus(Article::APPROVED);
            $em->persist($article);
            $em->flush();

            $contentWriter = $article->getContentWriter();
            $to            = $contentWriter->getEmail();
            $subject       = $translated->trans('article.name').' '.$article->getArticleTitle().$translated->trans('article.msg.approved');

            $mailer->send($to, $subject, 'EmailTemplates/article.html.twig', ['article' => $article, 'contentWriter' => $contentWriter]);
            $this->addFlash('success', $this->get('translator')->trans('page.customer.view_drafter_article.success.approved'));
        }

        // Redirect To Customer Dashboard
        return $this->redirectToRoute('view_article', ['id' => $article->getId()]);
    }

    /**
     * @Route("/{id}/request-for-unpublish", name="article_request_for_unpublish")
     * @Method("GET")
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Article $article
     *
     * @return RedirectResponse
     */
    public function requestForUnPublish(Article $article)
    {
        $em         = $this->getDoctrine()->getEntityManager();
        $translated = $this->get('translator');
        $mailer     = $this->get('front.mail_sender');
        $article->setStatus(Article::UNPUBLISHED_REQUESTED);

        $mailer->send(
            $article->getContentWriter()->getEmail(),
            $translated->trans('label.customer').' '.
            $article->getCompany()->getName().' '.
            $translated->trans('subject.unpublish_mail'),
            'EmailTemplates/request.html.twig',
            ['article' => $article, 'unpublish' => true]
        );

        $this->addFlash('success', $this->get('translator')->trans('customer.article.requested_sent_for_unpublishing.message'));

        $em->flush();

        return $this->redirectToRoute('article_list', ['id' => $article->getId()]);
    }

    /**
     * Change boost status.
     *
     * @Route("/{id}/boost_status", name="boost_status")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_CUSTOMER')")
     */
    public function boostStatusAction(Request $request, Article $article)
    {
        $em         = $this->getDoctrine()->getEntityManager();
        $translated = $this->get('translator');
        $session    = $request->getSession();
        $mailer     = $this->get('front.mail_sender');

        $boostStartDate = $request->request->get('start_date');
        if ($boostStartDate) {
            if ($article->getCompany()->getAvailableBoosts() <= 0) {
                $session->getFlashBag()->add(
                    'warning',
                    $translated->trans('boost.msg.enough')
                );

                return new JsonResponse(['data' => true]);
            }

//            $newDate        = date('Y-d-m', strtotime($boostStartDate));
            $boostStartDate = \DateTime::createFromFormat('d/m/Y', $boostStartDate);
//            $boostStartDate = new \DateTime($boostStartDate);

            if ($article->getCompany()->getStatus() == Company::ACTIVE) {
                $article->setBoostStartDate($boostStartDate);
                if ($article->getCompany()->getAvailableBoosts() > 0) {
                    $article->setBoostStatus(Article::BOOST_REQUEST_SENT);
                    $em->persist($article);
                    $em->flush();

                    $to = $article->getDevOp()->getEmail();

                    $mailer->send($to, $translated->trans('label.customer').' '.$article->getCompany()->getName().$translated->trans('subject.boost_mail'), 'EmailTemplates/request.html.twig', ['article' => $article, 'boost' => true]);
                }
                $session->getFlashBag()->add(
                    'success',
                    $translated->trans('boost.msg.active')
                );
            } else {
                $session->getFlashBag()->add(
                    'notice',
                    $translated->trans('boost.msg.not_active')
                );
            }
        }

        return new JsonResponse(['data' => true]);
    }

    /**
     * Change print status.
     *
     * @Route("/{id}/print_status", name="print_status")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_CUSTOMER')")
     */
    public function printStatusAction(Request $request, Article $article)
    {
        $em         = $this->getDoctrine()->getEntityManager();
        $translated = $this->get('translator');
        $session    = $request->getSession();
        $mailer     = $this->get('front.mail_sender');

        if ($article->getCompany()->getAvailablePrints() <= 0) {
            $session->getFlashBag()->add(
                'warning',
                $translated->trans('print.msg.not_available')
            );

            return $this->redirectToRoute('article_list');
        }

        if ($article->getCompany()->getStatus() == Company::ACTIVE) {
            if ($article->getCompany()->getAvailablePrints() > 0) {
                $article->setPrintStatus(Article::PRINT_REQUEST_SENT);
                $em->persist($article);
                $em->flush();

                $to = $article->getDevOp()->getEmail();

                $mailer->send($to, $translated->trans('subject.print_mail'), 'EmailTemplates/request.html.twig', ['article' => $article, 'print' => true]);
            }
            $session->getFlashBag()->add(
                'success',
                $translated->trans('print.msg.active')
            );
        } else {
            $session->getFlashBag()->add(
                'notice',
                $translated->trans('print.msg.not_active')
            );
        }

        return $this->redirectToRoute('article_list');
    }

    /**
     * @Route("/statistics", name="statistics")
     * @Security("has_role('ROLE_CUSTOMER')")
     */
    public function statistics()
    {
        return $this->render('FrontBundle:article:statistics.html.twig');
    }
}
