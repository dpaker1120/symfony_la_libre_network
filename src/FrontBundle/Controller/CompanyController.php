<?php

namespace FrontBundle\Controller;

use AdminBundle\Form\Type\CustomerEditFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Route("edit-company", name="company_edit")
     * @Security("has_role('ROLE_CUSTOMER')")
     * @Template("AdminBundle:Customer:edit.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $company = $this->getUser()->getCompany();

        $form = $this->createForm(CustomerEditFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($company);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('customer_edited'));

            return $this->redirectToRoute('company_profile');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
