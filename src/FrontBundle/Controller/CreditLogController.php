<?php

namespace FrontBundle\Controller;

use EntityBundle\Entity\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreditLogController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("view_credit_log", name="view_credit_log")
     * @Security("has_role('ROLE_REPRESENTATIVE')")
     *
     * @return Response
     */
    public function viewCreditLogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $company = $this->getUser()->getCompany();
        $logs = $em->getRepository('EntityBundle:CreditLog')->findByCompany($company)->getResult();

        return $this->render('FrontBundle:subscription:view_credit_log.html.twig', [
            'logs' => $logs,
            'company' => $company,
        ]);
    }
}