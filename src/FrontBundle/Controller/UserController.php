<?php

namespace FrontBundle\Controller;

use FrontBundle\Form\Type\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/profile", name="company_profile")
     * @Security("has_role('ROLE_CUSTOMER')")
     * @Template("FrontBundle:company:my_profile.html.twig")
     */
    public function indexAction()
    {
        $em      = $this->getDoctrine()->getManager();
        $company = $this->getUser()->getCompany();

        $admin          = $em->getRepository('EntityBundle:User')->findByRoleAndCompany($company, 'ROLE_CUSTOMER')->getOneOrNullResult();
        $representative = $em->getRepository('EntityBundle:User')->findByRoleAndCompany($company, 'ROLE_REPRESENTATIVE')->getResult();

        return [
            'company'         => $company,
            'admin'           => $admin,
            'representatives' => $representative,
        ];
    }

    /**
     * @Route("/edit-profile", name="user_profile")
     * @Route("/admin/edit-profile", name="admin_profile")
     */
    public function editProfile(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('user.msg.edit_profile'));

            if ($this->getUser()->hasAccess('ALL_USER') || $this->getUser()->hasAccess('ADMIN')) {
                return $this->redirectToRoute('admin_profile');
            } else {
                return $this->redirectToRoute('user_profile');
            }
        }

        $data = [
            'user' => $user,
            'form' => $form->createView(),
        ];

        if ($this->getUser()->hasAccess('ALL_USER') || $this->getUser()->hasAccess('ADMIN')) {
            return $this->render('AdminBundle:user:edit.html.twig', $data);
        } else {
            return $this->render('FrontBundle:user:edit.html.twig', $data);
        }
    }
}
