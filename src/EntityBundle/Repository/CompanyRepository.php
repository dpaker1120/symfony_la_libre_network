<?php

namespace EntityBundle\Repository;

use EntityBundle\Entity\Company;

class CompanyRepository extends \Doctrine\ORM\EntityRepository
{
    public function findCompaniesInWaitingPeriod()
    {
        $qb = $this->createQueryBuilder('company')
            ->where('company.status = :status')
            ->setParameter('status', Company::WAITING_PERIOD)
            ->andWhere('company.subscriptionEndDate < :now')
            ->setParameter('now', new \DateTime());

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all the customers whose subscription is near to end.
     *
     * @return Company[]
     */
    public function getCustomersWhoseSubscriptionIsNearToEnd()
    {
        $dateAfterTwoMonth = new \DateTime('+2 month');
        $dateAfterOneMonth = new \DateTime('+1 month');
        $qb                = $this->createQueryBuilder('company')
            ->where('company.isDndRegistered = false');

        return $qb->andWhere(
            $qb->expr()->orX(
                'company.subscriptionEndDate = :dateAfterTwoMonth',
                'company.subscriptionEndDate = :dateAfterOneMonth'
            )
        )->setParameters([
            'dateAfterTwoMonth' => $dateAfterTwoMonth->format('Y-m-d'),
            'dateAfterOneMonth' => $dateAfterOneMonth->format('Y-m-d'),
         ])->getQuery()
           ->getResult();
    }

    /**
     * Get all the customers whose subscription is end today.
     *
     * @return Company[]
     */
    public function getCustomerWhoseSubscriptionIsEndsToday()
    {
        $currentDate = new \DateTime();

        return $this->createQueryBuilder('company')
            ->where('company.subscriptionEndDate = :currentDate')
            ->setParameter('currentDate', $currentDate->format('Y-m-d'))
            ->getQuery()
            ->getResult();
    }

    /**
     * Get all the customers whose subscription already ended before 2 months.
     *
     * @return array
     */
    public function getCustomersWhoseSubscriptionIsAlreadyEndedBeforeTwoMonths()
    {
        $currentDate = new \DateTime('-2 month');

        return $this->createQueryBuilder('company')
            ->where('company.subscriptionEndDate = :currentDate')
            ->andWhere('company.status = :waitingStatus')
            ->setParameters([
                'currentDate'   => $currentDate->format('Y-m-d'),
                'waitingStatus' => Company::WAITING_PERIOD,
            ])
            ->getQuery()
            ->getResult();
    }
}
