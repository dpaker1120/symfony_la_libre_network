<?php

namespace EntityBundle\Repository;

class CreditLogRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByCompany($company)
    {
        $qb = $this->createQueryBuilder('cl')
            ->join('cl.companySubscriptionHistory', 'csh')
            ->where('csh.company = :company')
            ->setParameter('company', $company)
        ;

        return $qb->getQuery();
    }
}
