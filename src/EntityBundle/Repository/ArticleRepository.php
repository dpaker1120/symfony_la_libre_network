<?php

namespace EntityBundle\Repository;

use EntityBundle\Entity\Article;
use EntityBundle\Entity\Company;

class ArticleRepository extends \Doctrine\ORM\EntityRepository
{
    public function findUnpublishedArticles($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.status = :articleRequested')
            ->orWhere('a.status = :changeRequested')
            ->orWhere('a.status = :approved')
            ->setParameter('articleRequested', Article::ARTICLE_REQUESTED)
            ->setParameter('changeRequested', Article::CHANGE_REQUESTED)
            ->setParameter('approved', Article::APPROVED)
        ;

        return $qb->getQuery();
    }

    public function findPublishedArticles($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.status = :published')
            ->setParameter('published', Article::PUBLISHED)
        ;

        return $qb->getQuery();
    }

    public function articlesRequestedForBoost($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.boostStatus = :requested')
            ->setParameter('requested', Article::BOOST_REQUEST_SENT)
        ;

        return $qb->getQuery();
    }

    public function articlesRequestedForPrint($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.printStatus = :requested')
            ->setParameter('requested', Article::PRINT_REQUEST_SENT)
        ;

        return $qb->getQuery();
    }

    public function articlesRequestedForUnPublishing($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.status = :unpublising_requested')
            ->orWhere('a.status = :unpublising_requested_auto')
            ->setParameter('unpublising_requested', Article::UNPUBLISHED_REQUESTED)
            ->setParameter('unpublising_requested_auto', Article::UNPUBLISHED_REQUESTED_AUTO)
        ;

        return $qb->getQuery();
    }

    public function findForCustomerApprovals($company = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.company', 'c')
            ->where('c.status = :active')
            ->setParameter('active', Company::ACTIVE)
        ;

        if(null !== $company) {
            $qb
                ->andWhere('a.company = :company')
                ->setParameter('company', $company)
            ;
        }

        $qb
            ->andWhere('a.status = :articleDrafted')
            ->orWhere('a.status = :changeCompleted')
            ->setParameter('articleDrafted', Article::ARTICLE_DRAFTED)
            ->setParameter('changeCompleted', Article::CHANGE_COMPLETED)
        ;

        return $qb->getQuery();
    }
}
