<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\SubscriptionRepository")
 */
class Subscription
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $totalArticles;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $totalBoosts;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $totalPrints;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $totalConferences;

    /**
     * @var CompanySubscriptionHistory[]|ArrayCollection[]
     *
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\CompanySubscriptionHistory", mappedBy="subscription")
     */
    private $subscriptionHistory;

    public function __construct()
    {
        $this->subscriptionHistory = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     *
     * @return Subscription
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $totalArticles
     *
     * @return Subscription
     */
    public function setTotalArticles($totalArticles)
    {
        $this->totalArticles = $totalArticles;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalArticles()
    {
        return $this->totalArticles;
    }

    /**
     * @param int $totalBoosts
     *
     * @return Subscription
     */
    public function setTotalBoosts($totalBoosts)
    {
        $this->totalBoosts = $totalBoosts;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalBoosts()
    {
        return $this->totalBoosts;
    }

    /**
     * @param int $totalPrints
     *
     * @return Subscription
     */
    public function setTotalPrints($totalPrints)
    {
        $this->totalPrints = $totalPrints;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrints()
    {
        return $this->totalPrints;
    }

    /**
     * @param int $totalConferences
     *
     * @return Subscription
     */
    public function setTotalConferences($totalConferences)
    {
        $this->totalConferences = $totalConferences;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalConferences()
    {
        return $this->totalConferences;
    }

    /**
     * @return CompanySubscriptionHistory[]|ArrayCollection
     */
    public function getSubscriptionHistory()
    {
        return $this->subscriptionHistory;
    }
}
