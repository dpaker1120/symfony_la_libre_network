<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\CreditLogRepository")
 */
class CreditLog
{
    const MOVEMENT_TYPE_ADD        = 'add';
    const MOVEMENT_TYPE_CONSUME    = 'consume';
    const MOVEMENT_TYPE_CORRECTION = 'correction';

    const REASON_CODE_PURCHASE_PACK = 'purchase_pack';
    const REASON_CODE_RENEWAL       = 'renewal';
    const REASON_CODE_CONSUMPTION   = 'consumption';
    const REASON_CODE_COMPLAINT     = 'complaint';

    const CREDIT_TYPE_ARTICLE    = 'article';
    const CREDIT_TYPE_BOOST      = 'boost';
    const CREDIT_TYPE_PRINT      = 'print';
    const CREDIT_TYPE_CONFERENCE = 'conference';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CompanySubscriptionHistory
     *
     * @ORM\ManyToOne(targetEntity="CompanySubscriptionHistory")
     */
    private $companySubscriptionHistory;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $creditType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $movementType;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $reasonCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $exactReasonLog;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $oldBalance;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $newBalance;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param CompanySubscriptionHistory $companySubscriptionHistory
     *
     * @return CreditLog
     */
    public function setCompanySubscriptionHistory(CompanySubscriptionHistory $companySubscriptionHistory)
    {
        $this->companySubscriptionHistory = $companySubscriptionHistory;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanySubscriptionHistory()
    {
        return $this->companySubscriptionHistory;
    }

    /**
     * @param string $creditType
     *
     * @return CreditLog
     */
    public function setCreditType($creditType)
    {
        $this->creditType = $creditType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreditType()
    {
        return $this->creditType;
    }

    /**
     * @param \DateTime $date
     *
     * @return CreditLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $amount
     *
     * @return CreditLog
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $movementType
     *
     * @return CreditLog
     */
    public function setMovementType($movementType)
    {
        $this->movementType = $movementType;

        return $this;
    }

    /**
     * @return string
     */
    public function getMovementType()
    {
        return $this->movementType;
    }

    /**
     * @param string $reasonCode
     *
     * @return CreditLog
     */
    public function setReasonCode($reasonCode)
    {
        $this->reasonCode = $reasonCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    /**
     * @param string $exactReasonLog
     *
     * @return CreditLog
     */
    public function setExactReasonLog($exactReasonLog)
    {
        $this->exactReasonLog = $exactReasonLog;

        return $this;
    }

    /**
     * @return string
     */
    public function getExactReasonLog()
    {
        return $this->exactReasonLog;
    }

    /**
     * @return int
     */
    public function getOldBalance()
    {
        return $this->oldBalance;
    }

    /**
     * @param int $oldBalance
     *
     * @return CreditLog
     */
    public function setOldBalance($oldBalance)
    {
        $this->oldBalance = $oldBalance;

        return $this;
    }

    /**
     * @return int
     */
    public function getNewBalance()
    {
        return $this->newBalance;
    }

    /**
     * @param int $newBalance
     *
     * @return CreditLog
     */
    public function setNewBalance($newBalance)
    {
        $this->newBalance = $newBalance;

        return $this;
    }

    /**
     * @param Article $article
     * @param int     $oldBalance
     * @param string  $creditType
     *
     * @return CreditLog
     */
    public static function fromArticle(Article $article, $oldBalance, $creditType)
    {
        $company             = $article->getCompany();
        $creditLog           = new self();
        $subscriptionHistory = $company->getCurrentSubscription()->getSubscriptionHistory()->last();

        if ($subscriptionHistory) {
            $creditLog->setCompanySubscriptionHistory($subscriptionHistory);
        }

        return $creditLog
            ->setAmount(1)
            ->setCreditType($creditType)
            ->setDate(new \DateTime())
            ->setMovementType(self::MOVEMENT_TYPE_CONSUME)
            ->setReasonCode(self::REASON_CODE_CONSUMPTION)
            ->setOldBalance($oldBalance)
            ->setExactReasonLog('From CreditLog while change something in article.')
            ->setNewBalance($oldBalance - 1);
    }

    /**
     * @param Company $company
     * @param int     $oldBalance
     * @param int     $newBalance
     * @param int     $creditType
     * @param int     $amount
     *
     * @return CreditLog
     */
    public static function fromExtraCredit(Company $company, $oldBalance, $newBalance, $creditType, $amount)
    {
        $subscriptionHistory = $company->getCurrentSubscription()->getSubscriptionHistory()->last();
        $creditLog           = new self();

        if ($subscriptionHistory) {
            $creditLog->setCompanySubscriptionHistory($subscriptionHistory);
        }

        return $creditLog
            ->setAmount($amount)
            ->setCreditType($creditType)
            ->setDate(new \DateTime())
            ->setMovementType(self::MOVEMENT_TYPE_ADD)
            ->setReasonCode(self::REASON_CODE_PURCHASE_PACK)
            ->setOldBalance($oldBalance)
            ->setExactReasonLog('From Extra Credit log.')
            ->setNewBalance($newBalance);
    }
}
