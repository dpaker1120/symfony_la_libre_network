<?php

namespace EntityBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\CompanySubscriptionHistoryRepository")
 */
class CompanySubscriptionHistory
{
    const PAYMENT_PENDING                 = 'payment-pending';
    const PAYMENT_RECEIVED                = 'payment-received';
    const CUSTOMER_REQUESTED_CANCELLATION = 'customer-requested-cancellation';
    const PAYMENT_CANCELLED               = 'payment-cancelled';

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    public $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     */
    private $company;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="Subscription", inversedBy="subscriptionHistory")
     */
    private $subscription;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $auto;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $subscriptionStartDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $subscriptionEndDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Company $company
     *
     * @return CompanySubscriptionHistory
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Subscription $subscription
     *
     * @return CompanySubscriptionHistory
     */
    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param int $status
     *
     * @return CompanySubscriptionHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $auto
     *
     * @return CompanySubscriptionHistory
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAuto()
    {
        return $this->auto;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return CompanySubscriptionHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getSubscriptionStartDate()
    {
        return $this->subscriptionStartDate;
    }

    /**
     * @param DateTime $subscriptionStartDate
     *
     * @return CompanySubscriptionHistory
     */
    public function setSubscriptionStartDate(DateTime $subscriptionStartDate)
    {
        $this->subscriptionStartDate = $subscriptionStartDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getSubscriptionEndDate()
    {
        return $this->subscriptionEndDate;
    }

    /**
     * @param DateTime $subscriptionEndDate
     *
     * @return CompanySubscriptionHistory
     */
    public function setSubscriptionEndDate(DateTime $subscriptionEndDate)
    {
        $this->subscriptionEndDate = $subscriptionEndDate;

        return $this;
    }

    /**
     * @param Company $company
     *
     * @return CompanySubscriptionHistory
     */
    public static function fromCompany(Company $company)
    {
        $companySubscriptionHistory = new self();

        return $companySubscriptionHistory
            ->setCompany($company)
            ->setSubscription($company->getCurrentSubscription())
            ->setAuto(false)
            ->setStatus(self::PAYMENT_RECEIVED)
            ->setSubscriptionStartDate($company->getSubscriptionStartDate())
            ->setSubscriptionEndDate($company->getSubscriptionEndDate());
    }
}
