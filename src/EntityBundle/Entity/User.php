<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, groups={"user_create"})
 */
class User extends BaseUser
{
    const USER_GROUPS = [
        'ALL_USER' => [
            'ROLE_CONTENT_WRITER',
            'SALES_TEAM',
            'AD_OPS',
            'ROLE_LEAD_JOURNALIST',
            'ROLE_MANAGER',
            'ROLE_ACCOUNT_MANAGER',
        ],
        'ALL_USER_EXCEPT_LEAD_JOURNALIST' => [
            'ROLE_CONTENT_WRITER',
            'SALES_TEAM',
            'AD_OPS',
            'ROLE_MANAGER',
            'ROLE_ACCOUNT_MANAGER',
        ],
        'CONTENT_WRITER_AND_MANAGER' => [
            'ROLE_CONTENT_WRITER',
            'ROLE_MANAGER',
        ],
        'LEAD_JOURNALIST_AND_AD_OPS' => [
            'ROLE_LEAD_JOURNALIST',
            'AD_OPS',
        ],
        'SALES_TEAM_AND_MANAGER' => [
            'SALES_TEAM',
            'ROLE_MANAGER',
        ],
        'SALES_TEAM_AND_MANAGER_AND_ACCOUNT_MANAGER' => [
            'SALES_TEAM',
            'ROLE_MANAGER',
            'ROLE_ACCOUNT_MANAGER',
        ],
        'MANAGER_AND_ACCOUNT_MANAGER' => [
            'ROLE_MANAGER',
            'ROLE_ACCOUNT_MANAGER',
        ],
        'AD_OPS' => [
            'AD_OPS',
        ],
        'ADMIN' => [
            'ROLE_ADMIN'
        ]
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotNull(message="user_email.required", groups={"user_create"})
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $company;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $customer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="user_job_title.required", groups={"customer_add", "user_create"})
     */
    protected $jobTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotNull(message="user_name.required", groups={"user_create"})
     */
    protected $name;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Company $company
     *
     * @return User
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $jobTitle
     *
     * @return User
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function hasAccess($group)
    {
        $roles = self::USER_GROUPS[$group];

        $userRoles = $this->getRoles();

        foreach ($userRoles as $userRole) {
            if (in_array($userRole, $roles)) {
                return true;
            }
        }

        return false;
    }
}
