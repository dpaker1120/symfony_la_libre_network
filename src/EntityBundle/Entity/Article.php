<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\ArticleRepository")
 */
class Article
{
    const NOT_REQUESTED      = 'not_requested';
    const PRINT_REQUEST_SENT = 'print_request_sent';
    const BOOST_REQUEST_SENT = 'boost_request_sent';
    const ARTICLE_BOOSTED    = 'article_boosted';
    const ARTICLE_PRINTED    = 'article_printed';

    const ARTICLE_REQUESTED          = 'article_requested';
    const ARTICLE_DRAFTED            = 'article_drafted';
    const CHANGE_REQUESTED           = 'change_requested';
    const CHANGE_COMPLETED           = 'change_completed';
    const APPROVED                   = 'approved';
    const PUBLISHED                  = 'published';
    const UNPUBLISHED_REQUESTED      = 'unpublish_requested';
    const UNPUBLISHED                = 'unpublished';
    const UNPUBLISHED_REQUESTED_AUTO = 'unpublish_requested_auto';
    const UNPUBLISHED_AUTO           = 'unpublished_auto';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="articles")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $company;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotNull(message="Enterprise name is required.")
     * @ORM\Column(type="string", length=255)
     */
    private $enterpriseName;

    /**
     * @var string
     *
     * @Assert\NotNull(message="Turnover is required.")
     * @ORM\Column(type="string", length=255)
     */
    private $turnover;

    /**
     * @var int
     *
     * @Assert\NotNull(message="Number Of Employees is required.")
     * @ORM\Column(type="integer")
     */
    private $numberOfEmployees;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=500)
     */
    private $questions;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $contentWriter;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externalCmsLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $articleThumbnail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $articleTitle;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $status = self::NOT_REQUESTED;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $unPublishedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $boostStatus = self::NOT_REQUESTED;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $boostStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $boostedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $devOp;

    /**
     * @var int
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $printStatus = self::NOT_REQUESTED;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $printedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortDescription;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Company $company
     *
     * @return Article
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param User $user
     *
     * @return Article
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $enterpriseName
     *
     * @return Article
     */
    public function setEnterpriseName($enterpriseName)
    {
        $this->enterpriseName = $enterpriseName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnterpriseName()
    {
        return $this->enterpriseName;
    }

    /**
     * @param string $turnover
     *
     * @return Article
     */
    public function setTurnover($turnover)
    {
        $this->turnover = $turnover;

        return $this;
    }

    /**
     * @return string
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * @param int $numberOfEmployees
     *
     * @return Article
     */
    public function setNumberOfEmployees($numberOfEmployees)
    {
        $this->numberOfEmployees = $numberOfEmployees;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfEmployees()
    {
        return $this->numberOfEmployees;
    }

    /**
     * @param User $contentWriter
     *
     * @return Article
     */
    public function setContentWriter(User $contentWriter)
    {
        $this->contentWriter = $contentWriter;

        return $this;
    }

    /**
     * @return User
     */
    public function getContentWriter()
    {
        return $this->contentWriter;
    }

    /**
     * @param string $externalCmsLink
     *
     * @return Article
     */
    public function setExternalCmsLink($externalCmsLink)
    {
        $this->externalCmsLink = $externalCmsLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getExternalCmsLink()
    {
        return $this->externalCmsLink;
    }

    /**
     * @param string $articleThumbnail
     *
     * @return Article
     */
    public function setArticleThumbnail($articleThumbnail)
    {
        $this->articleThumbnail = $articleThumbnail;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticleThumbnail()
    {
        return $this->articleThumbnail;
    }

    /**
     * @param string $articleTitle
     *
     * @return Article
     */
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * @param string $status
     *
     * @return Article
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $boostStatus
     *
     * @return Article
     */
    public function setBoostStatus($boostStatus)
    {
        $this->boostStatus = $boostStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getBoostStatus()
    {
        return $this->boostStatus;
    }

    /**
     * @param \DateTime $boostedAt
     *
     * @return Article
     */
    public function setBoostedAt($boostedAt)
    {
        $this->boostedAt = $boostedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBoostedAt()
    {
        return $this->boostedAt;
    }

    /**
     * @param User $devOp
     *
     * @return Article
     */
    public function setDevOp(User $devOp)
    {
        $this->devOp = $devOp;

        return $this;
    }

    /**
     * @return User
     */
    public function getDevOp()
    {
        return $this->devOp;
    }

    /**
     * @param string $printStatus
     *
     * @return Article
     */
    public function setPrintStatus($printStatus)
    {
        $this->printStatus = $printStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrintStatus()
    {
        return $this->printStatus;
    }

    /**
     * @param \DateTime $printedAt
     *
     * @return Article
     */
    public function setPrintedAt($printedAt)
    {
        $this->printedAt = $printedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPrintedAt()
    {
        return $this->printedAt;
    }

    /**
     * @param string $questions
     *
     * @return Article
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return \DateTime
     */
    public function getUnPublishedAt()
    {
        return $this->unPublishedAt;
    }

    /**
     * @param \DateTime $unPublishedAt
     */
    public function setUnPublishedAt($unPublishedAt)
    {
        $this->unPublishedAt = $unPublishedAt;
    }

    /**
     * @param string $shortDescription
     *
     * @return Article
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param \DateTime $boostStartDate
     *
     * @return Article
     */
    public function setBoostStartDate($boostStartDate)
    {
        $this->boostStartDate = $boostStartDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBoostStartDate()
    {
        return $this->boostStartDate;
    }
}
