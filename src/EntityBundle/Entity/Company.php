<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="EntityBundle\Repository\CompanyRepository")
 */
class Company
{
    const ACTIVE         = 'active';
    const WAITING_PERIOD = 'waiting_period';
    const BLOCKED        = 'blocked';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="company_name.required")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="company_address_line_1.required")
     */
    private $addressLine1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="company_address_line_2.required")
     */
    private $addressLine2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="company_city.required")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="company_zip.required")
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="company_country.required")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="company_phone.required")
     */
    private $phone;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="Subscription")
     */
    private $currentSubscription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotNull(message="company_subscription_start_date.required")
     */
    private $subscriptionStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotNull(message="company_subscription_end_date.required")
     */
    private $subscriptionEndDate;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $availableArticles = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $availableBoosts = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $availablePrints = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $availableConferences = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Assert\NotNull(message="company_vat.required")
     */
    private $vat;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status = self::ACTIVE;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="company")
     */
    private $users;

    /**
     * @var ArrayCollection|User[]
     */
    private $companyRepresentatives;

    /**
     * @var Article[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Article", mappedBy="company")
     */
    private $articles;

    /**
     * @var Article[]|ArrayCollection
     */
    private $articleRequestedForBoost;

    /**
     * @var Article[]|ArrayCollection
     */
    private $articleRequestedForPrint;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isDndRegistered = false;

    public function __construct()
    {
        $this->users                  = new ArrayCollection();
        $this->companyRepresentatives = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $addressLine1
     *
     * @return Company
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine2
     *
     * @return Company
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param string $city
     *
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $state
     *
     * @return Company
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $zip
     *
     * @return Company
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $country
     *
     * @return Company
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Subscription $currentSubscription
     *
     * @return Company
     */
    public function setCurrentSubscription(Subscription $currentSubscription)
    {
        $this->currentSubscription = $currentSubscription;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getCurrentSubscription()
    {
        return $this->currentSubscription;
    }

    /**
     * @param \DateTime $subscriptionStartDate
     *
     * @return Company
     */
    public function setSubscriptionStartDate($subscriptionStartDate)
    {
        $this->subscriptionStartDate = $subscriptionStartDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSubscriptionStartDate()
    {
        return $this->subscriptionStartDate;
    }

    /**
     * @param \DateTime $subscriptionEndDate
     *
     * @return Company
     */
    public function setSubscriptionEndDate($subscriptionEndDate)
    {
        $this->subscriptionEndDate = $subscriptionEndDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSubscriptionEndDate()
    {
        return $this->subscriptionEndDate;
    }

    /**
     * @param int $availableArticles
     *
     * @return Company
     */
    public function setAvailableArticles($availableArticles)
    {
        $this->availableArticles = $availableArticles;

        return $this;
    }

    /**
     * @return int
     */
    public function getAvailableArticles()
    {
        return $this->availableArticles;
    }

    /**
     * @param int $availableBoosts
     *
     * @return Company
     */
    public function setAvailableBoosts($availableBoosts)
    {
        $this->availableBoosts = $availableBoosts;

        return $this;
    }

    /**
     * @return int
     */
    public function getAvailableBoosts()
    {
        return $this->availableBoosts;
    }

    /**
     * @param int $availablePrints
     *
     * @return Company
     */
    public function setAvailablePrints($availablePrints)
    {
        $this->availablePrints = $availablePrints;

        return $this;
    }

    /**
     * @return int
     */
    public function getAvailablePrints()
    {
        return $this->availablePrints;
    }

    /**
     * @param int $availableConferences
     *
     * @return Company
     */
    public function setAvailableConferences($availableConferences)
    {
        $this->availableConferences = $availableConferences;

        return $this;
    }

    /**
     * @return int
     */
    public function getAvailableConferences()
    {
        return $this->availableConferences;
    }

    /**
     * @param float $vat
     *
     * @return Company
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param User $user
     *
     * @return Company
     */
    public function addUser(User $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * @return ArrayCollection|User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return User|null
     */
    public function getAdmin()
    {
        foreach ($this->users as $user) {
            if (in_array('ROLE_CUSTOMER', $user->getRoles(), true)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getRepresentatives()
    {
        $representatives = [];
        foreach ($this->users as $user) {
            if (in_array('ROLE_REPRESENTATIVE', $user->getRoles(), true)) {
                $representatives[] = $user;
            }
        }

        return $representatives;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Company
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanyRepresentatives()
    {
        return $this->companyRepresentatives;
    }

    /**
     * @param User $user
     *
     * @return Company
     */
    public function addCompanyRepresentative(User $user)
    {
        $this->companyRepresentatives->add($user);

        return $this;
    }


    /**
     * @return ArrayCollection|Article[]
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param Company $company
     *
     * @return Company
     */
    public static function blocked(Company $company)
    {
        foreach ($company->getUsers() as $user) {
            $user->setEnabled(false);
        }

        return $company
            ->setStatus(self::BLOCKED)
            ->setAvailableArticles(0)
            ->setAvailableBoosts(0)
            ->setAvailableConferences(0)
            ->setAvailablePrints(0);
    }
    /**
     * @param Company      $company
     * @param Subscription $subscription
     *
     * @return Company
     */
    public static function renew(Company $company, Subscription $subscription)
    {
        return $company
            ->setStatus(self::ACTIVE)
            ->setAvailableArticles($company->getAvailableArticles() + $subscription->getTotalArticles())
            ->setAvailableBoosts($company->getAvailableBoosts() + $subscription->getTotalBoosts())
            ->setAvailablePrints($company->getAvailablePrints() + $subscription->getTotalPrints())
            ->setAvailableConferences($company->getAvailableConferences() + $subscription->getTotalConferences());
    }

    /**
     * @return ArrayCollection|Article[]
     */
    public function getArticleRequestedForBoost()
    {
        $articleRequestedForBoosts = $this->getArticles()->matching(
            Criteria::create()->andWhere(Criteria::expr()->eq('boostStatus', Article::BOOST_REQUEST_SENT))
        );

        $this->articleRequestedForBoost = $articleRequestedForBoosts;

        return $this->articleRequestedForBoost;
    }

    /**
     * @return ArrayCollection|Article[]
     */
    public function getArticleRequestedForPrint()
    {
        $articleRequestedForPrint = $this->getArticles()->matching(
            Criteria::create()->andWhere(Criteria::expr()->eq('printStatus', Article::PRINT_REQUEST_SENT))
        );

        $this->articleRequestedForPrint = $articleRequestedForPrint;

        return $this->articleRequestedForPrint;
    }

    /**
     * @return boolean
     */
    public function isDndRegistered()
    {
        return $this->isDndRegistered;
    }

    /**
     * @param boolean $isDndRegistered
     *
     * @return Company
     */
    public function setIsDndRegistered($isDndRegistered)
    {
        $this->isDndRegistered = $isDndRegistered;

        return $this;
    }

    public function isSubscriptionRunning()
    {
        $now = new \DateTime();

        if ($now >= $this->subscriptionStartDate && $now <= $this->subscriptionEndDate) {
            return true;
        }

        return false;
    }
}
